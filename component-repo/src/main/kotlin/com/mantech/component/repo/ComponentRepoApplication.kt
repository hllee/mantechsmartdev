package com.mantech.component.repo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ComponentRepoApplication {
}

fun main(args: Array<String>) {
    runApplication<ComponentRepoApplication>(*args)
}
