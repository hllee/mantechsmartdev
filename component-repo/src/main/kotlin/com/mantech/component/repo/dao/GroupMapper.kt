package com.mantech.i18n.dao

import com.mantech.component.repo.domain.Group
import org.apache.ibatis.annotations.*

@Mapper
interface GroupMapper {

    @Select("SELECT id, name FROM groups")
    fun findAllGroups(): List<Group>

    @Delete("DELETE FROM groups WHERE id=#{id}")
    fun deleteGroup(id: Int)

    @Insert("INSERT INTO groups(name)  VALUES (#{name})")
    @Options(useGeneratedKeys=true, keyProperty="id")
    fun addGroup(group: Group): Integer

}