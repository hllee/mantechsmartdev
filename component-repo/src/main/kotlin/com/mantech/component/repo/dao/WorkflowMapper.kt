package com.mantech.i18n.dao

import com.mantech.component.repo.domain.WorkflowContent
import org.apache.ibatis.annotations.*

@Mapper
interface WorkflowMapper {

    @Select("SELECT id, name, group, '' AS content FROM workflows")
    fun findAllWorkflows(): List<WorkflowContent>;

    @Select("SELECT id, name, group, content AS content FROM workflows WHERE id=#{id}")
    fun getWorkflow(id: Int): WorkflowContent;

    @Insert("INSERT INTO workflows(name, groupId, content)  VALUES (#{name}, #{groupId}, #{content})")
    @Options(useGeneratedKeys=true, keyProperty="id")
    fun addWorkflow(workflow: WorkflowContent): Integer


    @Delete("DELETE FROM workflow WHERE id=#{id}")
    fun deleteWorkflow(id: Int)

}