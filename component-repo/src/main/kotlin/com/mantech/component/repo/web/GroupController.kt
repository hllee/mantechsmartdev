package com.mantech.component.repo.web

import org.springframework.web.bind.annotation.*

@RestController
class GroupController {
    var workflowGroup = hashSetOf<String>("MCCS", "DRBD", "common", "test", "default")
    var componentTemplateGroup = hashSetOf<String>()


    @GetMapping("/groups")
    fun allGroup(@RequestParam(value = "type", defaultValue = "workflow") type: String) :Set<String> {
        when {
            "workflow".equals(type) -> return workflowGroup
            else -> return componentTemplateGroup
        }
    }


    @PostMapping("/groups")
    fun newGroup(@RequestParam(value = "type", defaultValue = "workflow") type: String, @RequestBody group:String) {
        when {
            "workflow".equals(type) -> workflowGroup.add(group)
            else -> componentTemplateGroup.add(group)
        }
    }


    @DeleteMapping("/groups")
    fun deleteGroup(@RequestParam(value = "type", defaultValue = "workflow") type: String, @RequestBody group:String) {
        when {
            "workflow".equals(type) -> workflowGroup.remove(group)
            else -> componentTemplateGroup.remove(group)
        }
    }

}