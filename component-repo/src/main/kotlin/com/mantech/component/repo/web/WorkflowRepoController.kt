package com.mantech.component.repo.web

import com.mantech.component.repo.domain.WorkflowContent
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("repo/workflow")
class WorkflowRepoController {
    var workflows = HashMap<String, WorkflowContent>()


    @GetMapping("/all")
    fun all() {
    }

    @PostMapping("/workflow")
    fun newGroup(@RequestBody workflow:WorkflowContent) {
        workflows.put(workflow.name, workflow)
    }


    @DeleteMapping("/workflow")
    fun deleteGroup(@RequestParam(value = "name") name: String) {
        workflows.remove(name)
    }
}