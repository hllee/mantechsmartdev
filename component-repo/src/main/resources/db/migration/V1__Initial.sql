create table "groups"
(
    "name" character varying(254),
    constraint "groups_pkey" primary key ("name")
);
create table "workflows"
(
    "group" character varying(254) not null references "groups"("name"),
    "name" character varying(254),
    "content" text
);
