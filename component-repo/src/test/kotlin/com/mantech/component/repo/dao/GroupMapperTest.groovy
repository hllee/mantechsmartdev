package com.mantech.component.repo.dao

import com.mantech.component.repo.domain.Group
import com.mantech.i18n.dao.GroupMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
class GroupMapperTest extends Specification {
    @Autowired
    GroupMapper groupMapper;

    def "test insertGroup"() {
        when:
        List<Group> groups = groupMapper.findAllGroups();

        then:
        groups.size() == 0

    }

}
