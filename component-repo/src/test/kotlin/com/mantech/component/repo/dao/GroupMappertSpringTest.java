package com.mantech.component.repo.dao;


import com.mantech.component.repo.domain.Group;
import com.mantech.i18n.dao.GroupMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@MybatisTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class GroupMappertSpringTest {
    @Autowired
    GroupMapper gropuMapper;

    @Test
    public void testInsertDeleteGroup() {
        List<Group> groups = gropuMapper.findAllGroups();

        Assert.assertTrue(groups.size() == 0);
    }

}
