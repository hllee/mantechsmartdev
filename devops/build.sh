cd ../i18n
docker build . -t i18nback
docker stop i18nback
docker rm i18nback
docker run -d -p 8081:8081 -e JAVA_OPTS=-Dserver.port=8081 --dns 10.10.0.3 --hostname i18nback --name i18nback i18nback

cd ../mantechI18nWeb
docker build . -t i18nweb
docker stop i18nweb
docker rm i18nweb
docker run -d -p 4200:4200 --hostname i18nweb --name i18nweb i18nweb

docker logs -f i18nweb
