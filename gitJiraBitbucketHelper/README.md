jira, git, bitbucket등의 연동을 위해 만들었다. 


# 사용법
1. 필요한 python library 설치.
pip3 install -r requirements.txt

2. config.ini수정.
user id, password 정보 수정.
BITBUCKET_COPY_PULLREQUEST_ID에 전에 pullrequest올렸던 id를 그대로 사용.

3. gitCommit.py 스크립트 실행.
./gitCommit.py


# ./gitCommit.py의 역할
1. git index에 있는 내용을 기반으로 commit. commit 내용은 jira issue의 title, 이슈내용에 의해 자동 생성된다.
2. bitbucket에 pull request 자동생성. reviewer은 config.ini에 있는 'BITBUCKET_COPY_PULLREQUEST_ID'에 있는 pullrequest의 reviewer로 자동추가.
3. jira issue 상태를 'Progress'에서 'Review'상태로 변경.


# gitCommit.py의 선제조건
1. git의 branch이름에 issue ID가 포함되어야 한다.

