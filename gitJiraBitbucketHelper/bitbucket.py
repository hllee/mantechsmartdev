#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#*******************************************************************************
# (c) 2018, Harold Lee <hllee@mantech.co.kr>
# Description: bitbucket api
# Example:
# /opt/work/scrpts/bitbucket.py -u hllee -P secret -r wind -R mantech -a list
# /opt/work/scrpts/bitbucket.py -u hllee -P secret -r wind -R mantech -a get -e /pullrequests/3783
# /opt/work/scrpts/bitbucket.py -u hllee -P secret -r wind -R mantech -a pullrequest -b hllee/VM-4487


import json

from http import HTTPStatus
import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import argparse
import sys
import os
import ntpath
import subprocess

from requests.auth import HTTPBasicAuth

def convertResponseToResultDic(r):
    if r.status_code != HTTPStatus.OK:
        result = {'code': r.status_code, 'output': '', 'error': r.text or ''}
    else:
        try:
            result = json.loads(r.text)
            #result = r.json
        except:
            print("*" * 30)
            exc_info = sys.exc_info()
            traceback.print_exception(*exc_info)
            print("*" * 30)
            result = {'code': 1, 'output': 'not valid json', 'error': r.text or ''}
    return result


def checkMeApproved(user, data):
    for item in data['values']:
        if 'approval' in item:
            if user == item['approval']['user']['username']:
                return True
    return False

class BitbucketAgent:
    def __init__(self, user, password, repo, repoOwner):
        self.user = user
        self.password = password
        self.baseUri = 'https://api.bitbucket.org/2.0/repositories/{}/{}'.format(repoOwner, repo)

    def connect(self):
        #print("connect to {}".format(self.baseUri))
        self.s = requests.Session()
        self.s.verify = False
        self.s.headers.update({'Accept': 'application/json', 'Content-Type': 'application/jfrom'})

    def doGet(self, endpoint, data):
        if data and len(data.items()) > 0:
            endpoint += "?"
            items = data.items()
            lastKey = items[len(items) - 1][0]
            for key, value in data.items():
                endpoint += key + "=" + value
                if key != lastKey:
                    endpoint += "&"

        if endpoint.startswith('http'):
            requestUrl = endpoint
        else:
            requestUrl = '{}{}'.format(self.baseUri, endpoint)
        # print('doGet:', requestUrl)
        r = self.s.get(requestUrl, auth=HTTPBasicAuth(self.user, self.password))
        return convertResponseToResultDic(r)


    def doPost(self, endpoint, data):
        self.s.headers.update({'Accept': 'application/json',
                               'Content-Type': 'application/json',
                               })
        requestUrl = '{}{}'.format(self.baseUri, endpoint)
        # print("doPost data:",data)
        r = self.s.post(requestUrl, verify=False, auth=HTTPBasicAuth(self.user, self.password), data=json.dumps(data))
        return convertResponseToResultDic(r)


    def listRequest(self):
        requests = []
        result = self.doGet("/pullrequests",{})
        for pullrequest in result['values']:
            approve_url = pullrequest['links']['activity']['href']
            approve_result = bitbucketAgent.doGet(approve_url,{})
            approved = checkMeApproved(args.user, approve_result)
            if not approved and args.user != pullrequest['author']['username']:
                # print("title:", pullrequest['title'])
                # print("state:", pullrequest['state'])
                # print("author:", pullrequest['author']['username'])
                requests.append({"title":pullrequest['title'], "href": pullrequest['links']['html']['href'], "author": pullrequest['author']['username']})
                #print(pullrequest)
                #print("reviewers", pullrequest['reviewers'])
                # print("href", pullrequest['links']['html']['href'])
        return requests


    def pull_request(self, branch, title, desc, reference_pull_request_id):
        # branch_data = self.doGet("/refs/branches/{}".format(branch),{})
        data = {}
        data["source"] = {"branch":{"name": branch}}
        data["destination"] = {"branch":{"name": "develop"}}
        past_pull_data = self.doGet("/pullrequests/{}".format(reference_pull_request_id),{})
        reviewers_data = []
        for reviwer in past_pull_data["reviewers"]:
            print(reviwer["username"])
            if self.user != reviwer["username"]:
                reviewers_data.append({"uuid":reviwer["uuid"]})
        data["reviewers"] = reviewers_data
        print(reviewers_data)
        data["close_source_branch"] = True
        data["title"] = title
        data["description"] = desc
        result = self.doPost("/pullrequests", data)
        print("result:", result)
        return result


    def close(self):
        self.s.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Manage Mdrm Agent")
    parser.add_argument("-P", "--password", metavar="password", help='agent session ID')

    parser.add_argument("-a", "--action", metavar="action", help='action for do job. example:version, get, post, multipart')
    parser.add_argument("-e", "--endpoint", metavar="endpoint", help='endpoint for request')
    parser.add_argument("-d", "--data", metavar="data", default="{}", nargs="?", help='json format data for requests')
    parser.add_argument("-b", "--branch", metavar="branch", nargs="?", help='branch for pull requests')
    parser.add_argument("-u", "--user", metavar="user", nargs="?", help='bitbucket user')
    parser.add_argument("-r", "--repo", metavar="repo", nargs="?", help='bitbucket repo name')
    parser.add_argument("-R", "--repoOwner", metavar="repoOwner", nargs="?", help='repo Owner')
    parser.add_argument("-t", "--title", metavar="title", nargs="?", help='pullrequest title')
    parser.add_argument("-D", "--desc", metavar="desc", nargs="?", help='pullrequest description')

    args = parser.parse_args()

    bitbucketAgent = BitbucketAgent(args.user, args.password, args.repo, args.repoOwner)
    bitbucketAgent.connect()
    data = json.loads(args.data)
    endpoint = args.endpoint
    result = None
    if args.action == "list":
        requests = bitbucketAgent.listRequest()
        for pullrequest in requests:
            print("href", pullrequest['href'])
            subprocess.run(["firefox", pullrequest['href']]) 
    elif args.action == "get":
        result = bitbucketAgent.doGet(endpoint, data)
        if result is not None:
            print(result)
    elif args.action == "post":
        result = bitbucketAgent.doPost(endpoint, data)
    elif args.action == "pullrequest":
        branch = args.branch
        bitbucketAgent.pull_request(branch, args.title, args.desc, '3772')
    else:
        print("-a option must be 'version' || 'get' || 'post' || 'multipart', nothing to do")

