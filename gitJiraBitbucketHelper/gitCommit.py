#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# (c) 2018, Harold Lee <hllee@mantech.co.kr>

from jira import JIRA
import re

import os
from git import Repo
import bitbucket
import gitHelper
import jiraHelper
import configparser

if __name__ == "__main__":

    dirname = os.path.dirname(__file__)
    config_filename = os.path.join(dirname, 'config.ini')

    config = configparser.ConfigParser()
    config.read(config_filename)

    path = config['git']['GIT_HOME']
    #path = os.getcwd()

    gitAgent = gitHelper.GitAgent(path)

    issue_key = gitAgent.issue_key
    print("current issue_key:", issue_key)
    jira_username = config['jira']['JIRA_USER_ID']
    jira_password = config['jira']['JIRA_PASSWORD']
    jiraAgent = jiraHelper.JiraAgent(jira_username, jira_password)

    content = jiraAgent.getCommitContent(issue_key)
    print(content)

    gitAgent.commit(content)
    gitAgent.push()

    jiraAgent.changeIssueStatus(issue_key, 'review')

    bitbucket_username = config['bitbucket']['BITBUCKET_USER_ID']
    bitbucket_password = config['bitbucket']['BITBUCKET_PASSWORD']
    bitbucket_repo = config['bitbucket']['BITBUCKET_REPO_NAME']
    bitbucket_repo_owner = config['bitbucket']['BITBUCKET_REPO_OWNER']
    bitbucket_ref_pull_request = config['bitbucket']['BITBUCKET_COPY_PULLREQUEST_ID']
    bitbucketAgent = bitbucket.BitbucketAgent(bitbucket_username, bitbucket_password, bitbucket_repo, bitbucket_repo_owner)
    bitbucketAgent.connect()
    bitbucketAgent.pull_request(gitAgent.branch, jiraAgent.getIssueTitie(issue_key), content, bitbucket_ref_pull_request)




