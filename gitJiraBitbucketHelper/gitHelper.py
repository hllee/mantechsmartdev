#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# (c) 2018, Harold Lee <hllee@mantech.co.kr>


from jira import JIRA
import re

import os
from git import Repo
import bitbucket

class GitAgent:
    def __init__(self, repoPath):
        self.repo = Repo(repoPath)
        self.branch = self.repo.active_branch.name
        self.issue_key = self.__getIssueKey()

    def __getIssueKey(self):
        re_search_result = re.search('([A-Z]+\-\d+)$', self.branch)

        if re_search_result:
            return re_search_result[1]
        else:
            print("no regxp match")
            return None

    def commit(self, commitMsg):
        index = self.repo.index
        index.commit(commitMsg)

    def push(self):
        remote = self.repo.remotes.origin
        remote.push(refspec="refs/heads/{}:refs/heads/{}".format(self.branch, self.branch))

if __name__ == "__main__":
    path = os.getcwd()
    gitAgent = GitAgent(path)
    print("current branch", gitAgent.branch)
    print("current issue_key:", gitAgent.issue_key)


