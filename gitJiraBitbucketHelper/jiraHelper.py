#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# (c) 2018, Harold Lee <hllee@mantech.co.kr>
# how to install: pip3 install jira
# refrence: https://jira.readthedocs.io/en/master/examples.html
# examples: ./jiraHelper.py -u hllee -P secret -a start 

from jira import JIRA
import re

import os
from git import Repo

import argparse
import gitHelper

class JiraAgent:
    def __init__(self, user, password):
        auth = (user, password)
        self.jira = JIRA(server='https://mantech.jira.com',auth=auth)

    def getCommitContent(self, issue_key):
        issue = self.jira.issue(issue_key)
        #print(issue.fields.reporter.displayName)
        description = issue.fields.description
        content = "{} {}".format(issue.key, issue.fields.summary)

        re_search_result = re.search('h5\.\s*<처리결과>\s+(.+)h5.\s*', description, re.MULTILINE|re.DOTALL)
        if re_search_result:
            #print(re_search_result[1])
            content += '\n\n'
            content += re_search_result[1]
        else:
            print("no regxp match")

        return content

    def changeIssueStatus(self, issue_key, status):
        issue = self.jira.issue(issue_key)
        #print(issue)
        issue_transition_id = '4'
        if status == 'start':
            issue_transition_id = '4'
        elif status == 'review':
            issue_transition_id = '771'
        self.jira.transition_issue(issue, issue_transition_id)


    def getIssueTitie(self, issue_key):
        issue = self.jira.issue(issue_key)
        #print(issue.fields.reporter.displayName)
        return "{} {}".format(issue.key, issue.fields.summary)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="ipmi power manager")
    parser.add_argument("-P", "--password", metavar="password", help='jira user password')
    parser.add_argument("-u", "--user", metavar="user", help='jira user id')
    parser.add_argument("-a", "--action", metavar="action", help='action for jira job. examples:start, review')

    args = parser.parse_args()

    path = os.getcwd()

    gitAgent = gitHelper.GitAgent(path)

    issue_key = gitAgent.issue_key
    print("current issue_key:", issue_key)
    jiraAgent = JiraAgent(args.user, args.password)

    jiraAgent.changeIssueStatus(issue_key, args.action)

