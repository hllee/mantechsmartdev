package com.mantech.i18n

import com.google.api.gax.rpc.ResourceExhaustedException
import com.google.cloud.storage.BlobId
import com.google.cloud.storage.BlobInfo
import com.google.cloud.storage.StorageOptions
import com.google.cloud.translate.v3beta1.*
import com.mantech.i18n.translate.TranslateItem
import com.mantech.i18n.translate.TranslateMethod
import com.univocity.parsers.common.record.Record
import com.univocity.parsers.tsv.TsvParser
import com.univocity.parsers.tsv.TsvParserSettings
import com.univocity.parsers.tsv.TsvWriter
import com.univocity.parsers.tsv.TsvWriterSettings
import io.grpc.StatusRuntimeException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.FileInputStream
import java.io.FileReader
import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.TimeUnit
import javax.annotation.PostConstruct


@Service
class GoogleI18nService {

    companion object {
        @Suppress("JAVA_CLASS_ON_COMPANION")
        @JvmStatic
        private val logger = LoggerFactory.getLogger(javaClass.enclosingClass)
    }

    var prefix: String = "google translate"
    var projectId: String = "i18n-trans-auto";
    var location: String = "us-central1" //""global"; //'us-central1'

    var transText = """번역내용""".trimIndent()

    @Value("\${i18n.source.lang}")
    var sourceLang: String = "ko"

    @Value("\${i18n.target.lang}")
    var targetLang: String = "en"


    lateinit var translationServiceClient: TranslationServiceClient
    val locationName = LocationName.newBuilder().setProject(projectId).setLocation(location).build()

    var gcsSource = GcsSource.newBuilder().setInputUri("gs://auto-trans/mantech_glossary.csv").build()
    var glossaryName = "auto-trans"
    var glossaryLocation = "projects/${projectId}/locations/us-central1/glossaries/${glossaryName}"

    var bucketName = "auto-trans"
    var storageSourceFileName = "source.tsv"
    var storageTargetFileName = "target.tsv"
    var sourceGcsUri = "gs://${bucketName}/source.tsv"
    var targetGcsUri = "gs://${bucketName}/${storageTargetFileName}/"

    var storage = StorageOptions.getDefaultInstance().getService();


    @PostConstruct
    fun init() {
        println("=========================");
        println("=========================");
        println("google i18n service inited ${sourceLang} => ${targetLang}");
        println("=========================");
        println("=========================");

        translationServiceClient = TranslationServiceClient.create()
        uploadGlossary();

    }

    fun uploadGlossary() {
        val languageCodesSet = Glossary.LanguageCodesSet.newBuilder().addLanguageCodes(sourceLang).addLanguageCodes(targetLang).build()

        var glossaryInputConfig = GlossaryInputConfig.newBuilder().setGcsSource(gcsSource).build()

        val glossaryName = GlossaryName.newBuilder()
                .setProject(projectId)
                .setLocation(location)
                .setGlossary(glossaryName)
                .build()

        try {
            val response = translationServiceClient
                    .deleteGlossaryAsync(glossaryName.toString())
                    .get(300, TimeUnit.SECONDS)

            System.out.format("Deleted: %s\n", response.name)
        } catch (e: Exception) {
            System.out.println(e.message)
        }

        val glossary = Glossary.newBuilder()
                .setLanguageCodesSet(languageCodesSet)
                .setInputConfig(glossaryInputConfig)
                .setName(glossaryName.toString())
                .build()
        val request = CreateGlossaryRequest.newBuilder()
                .setParent(locationName.toString())
                .setGlossary(glossary)
                .build()

        // Call the API
        val response = translationServiceClient.createGlossaryAsync(request).get(300, TimeUnit.SECONDS)
        val listGlossaryRequest = ListGlossariesRequest.newBuilder().setParent(locationName.toString()).build()
        var glossaryResponse =
                translationServiceClient.listGlossaries(listGlossaryRequest);

        println("glossary size:${glossaryResponse}")
        glossaryResponse.iterateAll().forEach({
            System.out.format("Name: %s\n", it.getName());
            System.out.format("Language Codes Set:\n");
            System.out.format(
                    "Source Language Code: %s\n",
                    it.getLanguageCodesSet().getLanguageCodesList().get(0));
            System.out.format(
                    "Target Language Code: %s\n",
                    it.getLanguageCodesSet().getLanguageCodesList().get(1));
            System.out.format("Input Uri: %s\n", it.getInputConfig().getGcsSource());
        })

    }

    fun translate(source: String): String {
        val translateTextGlossaryConfig = TranslateTextGlossaryConfig.newBuilder().setGlossary(glossaryLocation).build()


        val translateTextRequest = TranslateTextRequest.newBuilder()
                .setParent(locationName.toString())
                .setMimeType("text/html")
                .setSourceLanguageCode(sourceLang)
                .setTargetLanguageCode(targetLang)
                .addContents(source)
                .setGlossaryConfig(translateTextGlossaryConfig)
                .build()

        // Call the API
        try {
            val response = translationServiceClient.translateText(translateTextRequest)
            return getResultFromTranslateResponse(response)
        } catch (e: StatusRuntimeException) {
            TimeUnit.SECONDS.sleep(101)
            val response = translationServiceClient.translateText(translateTextRequest)
            return getResultFromTranslateResponse(response)
        } catch (e: ResourceExhaustedException) {
            TimeUnit.SECONDS.sleep(101)
            val response = translationServiceClient.translateText(translateTextRequest)
            return getResultFromTranslateResponse(response)
        }
    }

    fun getResultFromTranslateResponse(response: TranslateTextResponse): String {
        var translationList = response.glossaryTranslationsList
        var result = translationList.get(0).translatedText
        if (result != null && result.length > 0) {
            return result;
        } else {
            val text = response.getTranslations(0).translatedText
            logger.error("no glossary result is avalbale, using no glossary result instead:${text}")
            return text
        }
    }

    fun translate(source: Map<String, TranslateItem>, translateMethod: TranslateMethod): List<TranslateItem> {

        if (source.size == 0) {
            return ArrayList<TranslateItem>()
        }
        val glossaryName = GlossaryName.newBuilder()
                .setProject(projectId)
                .setLocation(location)
                .setGlossary(glossaryName)
                .build()

        val translateTextGlossaryConfig = TranslateTextGlossaryConfig.newBuilder().setGlossary(glossaryName.toString()).build()
        var sourceText = toTsvContent(source.values.iterator())


        val storage = StorageOptions.getDefaultInstance().getService()
        val blobId = BlobId.of(bucketName, storageSourceFileName)
        val blobInfo = BlobInfo.newBuilder(blobId).setContentType(translateMethod.contentType).build()
        var fileInput = FileInputStream(uploadTsvFilePath)
//        storage.create(blobInfo, fileInput)
        val blob = storage.create(blobInfo, sourceText.toByteArray(Charsets.UTF_8))


        val gcsSource = GcsSource.newBuilder().setInputUri(sourceGcsUri).build()
        val inputConfig = InputConfig.newBuilder().setGcsSource(gcsSource).setMimeType(translateMethod.contentType).build()
        var newFolderName = ThreadLocalRandom.current().nextLong().toString()
        var targetGcsUri = "gs://${bucketName}/${newFolderName}/"
        val gcsDestination = GcsDestination.newBuilder().setOutputUriPrefix(targetGcsUri).build()
        val outputConfig = OutputConfig.newBuilder().setGcsDestination(gcsDestination).build()
        val batchTranslateTextRequest = BatchTranslateTextRequest.newBuilder()
                .setParent(locationName.toString())
                .setSourceLanguageCode(sourceLang)
                .addTargetLanguageCodes(targetLang)
                .addInputConfigs(inputConfig)
                .setOutputConfig(outputConfig)
                .putGlossaries("en", translateTextGlossaryConfig)
                .build()

        // Call the API
        val response = translationServiceClient
                .batchTranslateTextAsync(batchTranslateTextRequest)
                .get(300, TimeUnit.SECONDS)

        var results = getStorageTranslatedResult(newFolderName)
        results.forEach({
            var origin = source.get(it.itemId)
            if(origin != null) {
                it.translateId = origin.translateId
                it.elementType = origin.elementType
                it.origin = origin.origin
            }
        })

        return results

    }

    var uploadTsvFilePath = "/tmp/i18nupload.tsv"
    var downloadTsvFilePath = "/tmp/i18ndownload.tsv"
    fun getStorageTranslatedResult(blobFolderName: String): java.util.ArrayList<TranslateItem> {
        val blob = storage.get(BlobId.of(bucketName, "${blobFolderName}/mantechglossary_source_en_translations.tsv"))
        blob.downloadTo(Paths.get(downloadTsvFilePath))
        return getTsvContent()
    }

    fun getTsvContent(): java.util.ArrayList<TranslateItem> {
//        var inFile = Files.newBufferedReader(Paths.get(downloadTsvFilePath));
        var inFile = FileReader(downloadTsvFilePath);
        val settings = TsvParserSettings()
        settings.inputBufferSize = 999999
        settings.maxCharsPerColumn = 999999
        val parser = TsvParser(settings)
        var translateItems = java.util.ArrayList<TranslateItem>()
        parser.beginParsing(inFile)
        var record: Record? = parser.parseNextRecord()
        while (record != null) {
            if(record.values.size > 3 && record.getString(3) != null) {
                translateItems.add(TranslateItem(record.getString(0), record.getString(3)))
            } else {
                logger.error("record translate wroing ${record}")
                if(record.values.size >= 3 && record.getString(2) != null) {
                    translateItems.add(TranslateItem(record.getString(0), record.getString(2)))
                }
            }
            record = parser.parseNextRecord()
        }
        return translateItems
    }

    fun toTsvContent(source: Iterator<TranslateItem>): String {
        var writer = Files.newBufferedWriter(Paths.get(uploadTsvFilePath))
        val settings = TsvWriterSettings()
        var tsvWriter = TsvWriter(writer, settings)

        source.forEach({
            tsvWriter.writeRow(it.itemId, it.content.replace("\t", ""))
        })
        tsvWriter.close()
        return Files.readAllBytes(Paths.get(uploadTsvFilePath)).contentToString();
    }
}
