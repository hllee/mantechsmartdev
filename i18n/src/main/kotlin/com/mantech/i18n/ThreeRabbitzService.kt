package com.mantech.i18n

import com.mantech.i18n.dao.BookMapper
import com.mantech.i18n.dao.ChapterElementMapper
import com.mantech.i18n.dao.ChapterMapper
import com.mantech.i18n.dao.TranslateHistoryMapper
import com.mantech.i18n.entity.Chapter
import com.mantech.i18n.entity.ChapterElement
import com.mantech.i18n.translate.ElementType
import com.mantech.i18n.translate.TranslateHistory
import com.mantech.i18n.translate.TranslateItem
import com.mantech.i18n.translate.TranslateMethod
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service
import java.util.concurrent.ThreadLocalRandom
import javax.annotation.PostConstruct


@Service
//class ThreeRabbitzService(var bookMapper: BookMapper, var chapterElementMapper: ChapterElementMapper, var chapterMapper: ChapterMapper, var googleI18nService: GoogleI18nService, var translateHistoryMapper: TranslateHistoryMapper, val messagingTemplate: SimpMessageSendingOperations) {
class ThreeRabbitzService(var bookMapper: BookMapper, var chapterElementMapper: ChapterElementMapper, var chapterMapper: ChapterMapper, var googleI18nService: GoogleI18nService, var translateHistoryMapper: TranslateHistoryMapper, val messagingTemplate: SimpMessagingTemplate) {

    private val websocketTopic = "/events/translate";

    @PostConstruct
    fun init() {
//        googleI18nService.uploadGlossary()
//        reformatHtmlTag("5f9303ecca2d8472")
    }

    fun translateBook(id: String) {
        System.out.println("translatebook ${id}")
        var book = bookMapper.getBookInfo(id)
        var translateId = getTranslateId()
        var translateHistory = TranslateHistory(translateId, book.title)
        translateHistoryMapper.addTranslateHistory(translateHistory)


        var chapters = chapterMapper.findChapterById(id)
        var needTranlatedElements = HashMap<String, TranslateItem>()
        this.messagingTemplate.convertAndSend(websocketTopic, "get database table for ${book.title} finished");

        chapters.forEach {
            var chapterId = it.id
            println("chapter ${chapterId} => ${it.title}")
            this.messagingTemplate.convertAndSend(websocketTopic, "analysis data for chapter ${book.title} started");
            var chapterItems = getTranslateItemsFromChapter(it, translateId)
            needTranlatedElements.putAll(chapterItems)
            this.messagingTemplate.convertAndSend(websocketTopic, "analysis data for chapter ${book.title} finished");
        }
        translateItems(needTranlatedElements)
//        TimeUnit.SECONDS.sleep(30)
        this.messagingTemplate.convertAndSend(websocketTopic, "translate ${book.title} finished");
        println("translate finished: ${id}")
    }


    private var LINE_BREAK_REPLACE_CONTENT = " 3RABBITZLINEBREAK "
    var LINE_BREAK_REPLACE_CONTENT_REGEXP = Regex("\\s*3RABBITZLINEBREAK\\s*")
    fun translateChapter(id: String) {
        System.out.println("translateChapter ${id}")
        var chapter = chapterMapper.getChapterInfo(id)
        var translateId = getTranslateId()
        var translateHistory = TranslateHistory(translateId, chapter.title)
        translateHistoryMapper.addTranslateHistory(translateHistory)
        var chapterItems = getTranslateItemsFromChapter(chapter, translateId)
        translateItems(chapterItems)
        println("translate chapter finished: ${id}")
    }

    fun getTranslateItemsFromChapter(chapter: Chapter, translateId: String): HashMap<String, TranslateItem> {
        var needTranlatedElements = HashMap<String, TranslateItem>()
        if (!isEnglishSentence(chapter.title)) {
            var chapterItem = TranslateItem(chapter.id, chapter.title)
            chapterItem.elementType = ElementType.Chapter
            chapterItem.translateId = translateId
            chapterItem.origin = chapter.title
            needTranlatedElements.put(chapter.id, chapterItem)
        }

        var elements = chapterElementMapper.findChapterElementById(chapter.id)
        for (element in elements) {
            if (!isEnglishSentence(element.content)) {
                var translateItem = getTranslateItem(element, translateId)
                needTranlatedElements.put(translateItem.itemId, translateItem)
            }
        }
        return needTranlatedElements
    }

    fun translateItems(needTranlatedElements: HashMap<String, TranslateItem>) {
        var batchItems = HashMap<String, TranslateItem>()
        var translatedItems= ArrayList<TranslateItem>();
        needTranlatedElements.forEach {
            var content = it.value.content
            if(content.length < 99 || !content.contains('\n')) {
//            if(!content.contains('\n')) {
                if(batchItems.size < 29) {
                    batchItems.put(it.key, it.value)
                } else {
                    try {
                        var batchTranslatedItems = googleI18nService.translate(batchItems, TranslateMethod.HTML)
                        translatedItems.addAll(batchTranslatedItems)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        println("batch translate error:"+e.message);
                        for(notTranslatedItem:TranslateItem in batchItems.values) {
                            translateItem(notTranslatedItem);
                            translatedItems.add(notTranslatedItem)
                        }
                    }
                    batchItems.clear();
                }
            } else {
                translateItem(it.value)
                translatedItems.add(it.value)
            }

        }

        try {
            var batchTranslatedItems = googleI18nService.translate(batchItems, TranslateMethod.HTML)
            translatedItems.addAll(batchTranslatedItems)
        } catch (e: Exception) {
            e.printStackTrace()
            println("batch translate error:"+e.message);
        }

        this.messagingTemplate.convertAndSend(websocketTopic, "translate using google translation batch api finished");
        this.messagingTemplate.convertAndSend(websocketTopic, "update 3rabbitz table");
        for (element in translatedItems) {
            var content = element.content
            element.content = replaceWrongContent(content)
            if(element.elementType == ElementType.Element) {
                chapterElementMapper.updateChapterElementById(ChapterElement(element.itemId, element.content))
            } else if(element.elementType == ElementType.Chapter) {
                element.content = element.content.capitalize()
                chapterMapper.updateChapter(Chapter(element.itemId, element.content))
            }
            translateHistoryMapper.deleteTranslateItems(element.itemId)
            translateHistoryMapper.addTranslateItem(element)
        }
    }

    fun translateItem(item: TranslateItem): TranslateItem {
        var originContent = item.content
        var resultContent = replaceWrongContent(googleI18nService.translate(originContent))
        if(resultContent.length > 0) {
            item.content = resultContent
        } else {
            item.content = item.origin
        }
        return item
    }

    private val MSB :Long = 0x800000000000000L;
    fun getTranslateId() :String {
        var random = ThreadLocalRandom.current()
         return java.lang.Long.toHexString(MSB or random.nextLong()) + java.lang.Long.toHexString(MSB or random.nextLong());
    }

    fun reformatHtmlTag(id: String) {
        var chapters = chapterMapper.findChapterById(id)
        chapters.forEach {
            var chapterId = it.id
            println("chapter ${chapterId} => ${it.title}")

            var output = replaceWrongContent(it.title)
            if (!it.title.equals(output)) {
                it.title = output
                chapterMapper.updateChapter(it)
            }


            var elements = chapterElementMapper.findChapterElementById(chapterId)
            for (element in elements) {
                var output = replaceWrongContent(element.content)
                if (!element.content.equals(output)) {
                    element.content = output
                    chapterElementMapper.updateChapterElementById(element)
                }
            }
        }
    }

    private var ASCII_REGEXP = Regex("^[\\u0000-\\u007F]*$")
    private var HTML_CLOSE_TAG_REGEXP = Regex("<\\s*/\\s+([a-zA-Z]*)\\s*>")
    fun isEnglishSentence(sentence: String): Boolean {
        return sentence.matches(ASCII_REGEXP);
    }

    private var NEW_LINE_REGEXP = Regex("\\s+\\\\\\s+[Nn]\\s+")
    fun containsWrongNewLine(sentence: String): Boolean {
        return sentence.contains(NEW_LINE_REGEXP)
    }

    fun replaceWrongNewLine(sentence: String): String {
//        return NEW_LINE_REGEXP.replace(sentence, "\n");
        return NEW_LINE_REGEXP.replace(sentence, "\\\\n");
    }

    private var SPECIAL_CHARACTER_REGEXP = Regex("\\s*&\\s+([a-z]+)\\s*;")
    fun containsSpecialCharacter(sentence: String): Boolean {
        return sentence.contains(SPECIAL_CHARACTER_REGEXP)
    }

    fun replaceSpecialCharacter(sentence: String): String {
        return SPECIAL_CHARACTER_REGEXP.replace(sentence, "&$1;");
    }

    private var IMGTAG_REGEXP = Regex("<img\\s+src\\s*=\\s*/\\s*r\\s*/\\s*image\\s*/\\s*get\\s*/\\s*([a-zA-Z0-9]+)\\s* width\\s*=\\s*(\\d+) height\\s*=\\s*(\\d+)\\s*/>")
    fun containsWrongImgTag(sentence: String): Boolean {
        return sentence.contains(IMGTAG_REGEXP)
    }

    fun replaceWrongImgTag(sentence: String): String {
        var afterAmp = IMGTAG_REGEXP.replace(sentence, "<img src=/r/image/get/$1 width=$2 height=$3 />");
        return afterAmp
    }

    private var ALT_REGEXP = Regex("alt=\\S+\\s창에서\\s링크\\s열기")
    private var QUOTED_ALT_REGEXP = Regex("alt=\"\\S+\\s창에서\\s링크\\s열기\"")
    fun replaceAltAttribute(sentence: String): String {
        var afterAmp = QUOTED_ALT_REGEXP.replace(sentence,"")
        afterAmp = ALT_REGEXP.replace(afterAmp,"")
        return afterAmp
    }

    fun replaceWrongContent(content: String): String {
        var output = content;
        if (containsWrongNewLine(content)) {
            output = replaceWrongNewLine(content)
        }
        if (containsSpecialCharacter(output)) {
            output = replaceSpecialCharacter(output)
        }
        if (containsWrongImgTag(output)) {
            output = replaceWrongImgTag(output)
        }
        output = LINE_BREAK_REPLACE_CONTENT_REGEXP.replace(output, "\n")
        if (HTML_CLOSE_TAG_REGEXP.containsMatchIn(output)) {
            output = HTML_CLOSE_TAG_REGEXP.replace(output, "</$1>")
        }
        return output
    }

    fun getTranslateItem(element: ChapterElement, translateId: String) :TranslateItem {
        var translateItem = TranslateItem(element.id, element.content)
        translateItem.origin = element.content
        if(element.content.contains('\n')) {
            translateItem.content = element.content.replace("\n", LINE_BREAK_REPLACE_CONTENT)
        }
        translateItem.content = replaceAltAttribute(translateItem.content)
        translateItem.translateId = translateId
        return translateItem
    }
}

