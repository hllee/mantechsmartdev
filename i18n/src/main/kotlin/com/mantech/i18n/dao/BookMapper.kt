package com.mantech.i18n.dao

import com.mantech.i18n.entity.Book
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Select

@Mapper
interface BookMapper {

    @Select("SELECT B.R_ID AS id, B.R_TITLE AS title, P.R_NAME AS project, U.R_NAME AS creator, B.R_UPDATE_TIME AS updated  FROM  \"DATABASE\".\"R_BOOK\" AS B LEFT JOIN \"DATABASE\".\"R_PROJECT\" AS P ON B.R_PROJECT_ID = P.R_ID LEFT JOIN \"DATABASE\".\"R_USER\" AS U ON B.R_CREATOR = U.R_USERID WHERE B.R_TITLE IS NOT NULL")
    fun findAllBooks(): List<Book>

    @Select("SELECT B.R_ID AS id, B.R_TITLE AS title, P.R_NAME AS project, U.R_NAME AS creator, B.R_UPDATE_TIME AS updated  FROM  \"DATABASE\".\"R_BOOK\" AS B LEFT JOIN \"DATABASE\".\"R_PROJECT\" AS P ON B.R_PROJECT_ID = P.R_ID LEFT JOIN \"DATABASE\".\"R_USER\" AS U ON B.R_CREATOR = U.R_USERID WHERE B.R_TITLE IS NOT NULL AND B.R_ID=#{id}")
    fun getBookInfo(id: String): Book
}