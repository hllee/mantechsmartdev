package com.mantech.i18n.dao

import com.mantech.i18n.entity.ChapterElement
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Select
import org.apache.ibatis.annotations.Update

@Mapper
interface ChapterElementMapper {

    @Select("SELECT R_ID AS id, R_CONTENT AS content FROM  \"DATABASE\".\"R_CHAPTER_ELEMENT\" WHERE R_CHAPTER_ID=#{id} AND R_CONTENT IS NOT NULL")
    fun findChapterElementById(id: String): List<ChapterElement>;

//    @Select("SELECT R_ID, R_ALIAS  FROM  \"DATABASE\".\"R_CHAPTER_ELEMENT_R\" WHERE R_CHAPTER_ID=#{id} AND R_ALIAS IS NOT NULL")
//    fun findChapterAliasById(id: String): List<ChapterAlias>;

    @Update("UPDATE \"DATABASE\".\"R_CHAPTER_ELEMENT\" SET R_CONTENT = #{content} where R_ID=#{id}")
    fun updateChapterElementById(element: ChapterElement): Integer;

//    @Update("UPDATE \"DATABASE\".\"R_CHAPTER_ELEMENT_R\" SET R_ALIAS = #{R_ALIAS} where R_ID=#{R_ID}")
//    fun updateChapterAliasById(element: ChapterAlias): Integer;

}