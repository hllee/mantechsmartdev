package com.mantech.i18n.dao

import com.mantech.i18n.entity.Chapter
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Select
import org.apache.ibatis.annotations.Update

@Mapper
interface ChapterMapper {

    @Select("SELECT R_ID as id, R_TITLE AS title FROM  \"DATABASE\".\"R_CHAPTER\" WHERE R_BOOK_ID=#{id} AND R_TITLE IS NOT NULL")
    fun findChapterById(id: String): List<Chapter>

    @Update("UPDATE \"DATABASE\".\"R_CHAPTER\" SET R_TITLE = #{title} where R_ID=#{id}")
    fun updateChapter(chapter: Chapter): Integer

    @Select("SELECT R_ID as id, R_TITLE AS title FROM  \"DATABASE\".\"R_CHAPTER\" WHERE R_ID=#{id} AND R_TITLE IS NOT NULL")
    fun getChapterInfo(id: String): Chapter

}