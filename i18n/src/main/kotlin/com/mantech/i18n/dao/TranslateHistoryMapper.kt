package com.mantech.i18n.dao

import com.mantech.i18n.translate.TranslateHistory
import com.mantech.i18n.translate.TranslateItem
import org.apache.ibatis.annotations.*

@Mapper
interface TranslateHistoryMapper {

    @Select("SELECT ID AS translateId, TITLE AS name, DATE AS date FROM \"TRANSLATE_HISTORY\"")
    fun findAllTranslateHistorys(): List<TranslateHistory>;

    @Insert("INSERT INTO \"TRANSLATE_HISTORY\"(ID, TITLE, DATE)  VALUES (#{translateId}, #{name}, #{date})")
    fun addTranslateHistory(translateHistory: TranslateHistory): Integer

    @Select("SELECT ITEMID AS itemId, CONTENT AS content, ORIGIN AS origin, TRANSLATEID AS translateId, ELEMENTTYPE AS elementType FROM \"TRANSLATE_ITEM\" WHERE TRANSLATEID=#{id}")
    fun findTranslateItems(id: String): List<TranslateItem>;

    @Select("SELECT I.ITEMID AS itemId, I.CONTENT AS content, I.ORIGIN AS origin, I.TRANSLATEID AS translateId, I.ELEMENTTYPE AS elementType FROM \"TRANSLATE_ITEM\" AS I,  \"DATABASE\".\"R_CHAPTER_ELEMENT\" AS E WHERE E.R_CHAPTER_ID=#{id} AND E.R_ID=I.ITEMID")
    fun findTranslateItemsByChapter(id: String): List<TranslateItem>;


    @Select("SELECT ITEMID AS itemId, CONTENT AS content, ORIGIN AS origin, TRANSLATEID AS translateId, ELEMENTTYPE AS elementType FROM \"TRANSLATE_ITEM\" WHERE ITEMID=#{id}")
    fun getTranslateItem(id: String): TranslateItem;

    @Insert("INSERT INTO \"TRANSLATE_ITEM\"(ITEMID, CONTENT, ORIGIN, TRANSLATEID, ELEMENTTYPE)  VALUES (#{itemId}, #{content}, #{origin}, #{translateId}, #{elementType})")
    fun addTranslateItem(translateItem: TranslateItem): Integer


    @Delete("DELETE FROM \"TRANSLATE_HISTORY\" WHERE ID=#{id}")
    fun deleteTranslateHistorys(id: String)

    @Delete("DELETE FROM \"TRANSLATE_ITEM\" WHERE ITEMID=#{id}")
    fun deleteTranslateItems(id: String)

    @Update("UPDATE \"DATABASE\".\"R_CHAPTER_ELEMENT\" SET R_CONTENT = #{origin} where R_ID=#{itemId}")
    fun revertTranslateElement(translateItem: TranslateItem): Integer;


    @Update("UPDATE \"DATABASE\".\"R_CHAPTER\" SET R_TITLE = #{origin} where R_ID=#{itemId}")
    fun revertTranslateChapter(translateItem: TranslateItem): Integer;

}