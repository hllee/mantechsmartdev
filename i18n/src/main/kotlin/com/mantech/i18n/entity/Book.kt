package com.mantech.i18n.entity

import java.util.*

class Book(var id: String, var title: String, var project: String?, var creator: String?, var updated: Date)
