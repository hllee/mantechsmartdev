package com.mantech.i18n.translate

enum class ElementType {
    Chapter,
    Element
}