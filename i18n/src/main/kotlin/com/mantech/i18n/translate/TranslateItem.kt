package com.mantech.i18n.translate

class TranslateItem(var itemId: String, var content: String) {
    open var elementType: ElementType = ElementType.Element
    var origin: String = ""
    var translateId: String = ""
}