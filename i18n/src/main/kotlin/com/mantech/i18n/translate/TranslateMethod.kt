package com.mantech.i18n.translate

enum class TranslateMethod(var contentType: String) {
    TEXT("text/plain"),
    HTML("text/html")
}