package com.mantech.i18n.web

import com.mantech.i18n.ThreeRabbitzService
import com.mantech.i18n.entity.Book
import com.mantech.i18n.dao.BookMapper
import com.mantech.i18n.dao.ChapterElementMapper
import com.mantech.i18n.dao.ChapterMapper
import com.mantech.i18n.entity.Chapter
import com.mantech.i18n.entity.ChapterElement
import org.springframework.web.bind.annotation.*

@RestController
class ThreeRabbitzController(val bookMapper: BookMapper, val chapterMapper: ChapterMapper, val chapterElementMapper: ChapterElementMapper, val threeRabbitzService: ThreeRabbitzService) {


    @CrossOrigin
    @GetMapping("/books")
    fun getBooks(): List<Book> {
        return bookMapper.findAllBooks()
    }

    @CrossOrigin
    @GetMapping("/books/{id}")
    fun getBook(@PathVariable("id") id: String): Book {
        return bookMapper.getBookInfo(id)
    }

    @CrossOrigin
    @PostMapping("/translatebook/{id}")
    fun translateBook(@PathVariable("id") id: String) :List<Chapter>{
        threeRabbitzService.translateBook(id);
        return chapterMapper.findChapterById(id)
    }

    @CrossOrigin
    @GetMapping("/chaptersByBook/{id}")
    fun listChapter(@PathVariable("id") id: String) :List<Chapter>{
        return chapterMapper.findChapterById(id)
    }

    @CrossOrigin
    @GetMapping("/chapters/{id}")
    fun getChapter(@PathVariable("id") id: String) :Chapter{
        return chapterMapper.getChapterInfo(id)
    }

    @CrossOrigin
    @GetMapping("/elementsByChapter/{id}")
    fun listElement(@PathVariable("id") id: String) :List<ChapterElement>{
        return chapterElementMapper.findChapterElementById(id)
    }

    @CrossOrigin
    @PostMapping("/translatechapter/{id}")
    fun translateChapter(@PathVariable("id") id: String) :List<Chapter>{
        threeRabbitzService.translateChapter(id);
        return listOf()
    }


}