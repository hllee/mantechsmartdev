package com.mantech.i18n.web

import com.mantech.i18n.dao.TranslateHistoryMapper
import com.mantech.i18n.translate.ElementType
import com.mantech.i18n.translate.TranslateHistory
import com.mantech.i18n.translate.TranslateItem
import org.springframework.web.bind.annotation.*
import java.lang.Exception

@RestController
class TranslateHistoryController(val translateHistoryMapper: TranslateHistoryMapper) {


    @CrossOrigin
    @PostMapping("/revertTranslateItem")
    fun revertTranslateItem(@RequestBody translateItem: TranslateItem): Boolean {
        translateHistoryMapper.revertTranslateElement(translateItem)
        return true
    }


    @CrossOrigin
    @GetMapping("/translateHistorys")
    fun listTranslateHistorys(): List<TranslateHistory> {
        return translateHistoryMapper.findAllTranslateHistorys()
    }

    @CrossOrigin
    @GetMapping("/translateItemsByChapter/{id}")
    fun translateItemsByChapter(@PathVariable("id") id: String): List<TranslateItem> {
        return translateHistoryMapper.findTranslateItemsByChapter(id)
    }

    @CrossOrigin
    @GetMapping("/translateItems/{id}")
    fun listTranslateItems(@PathVariable("id") id: String): List<TranslateItem> {
        return translateHistoryMapper.findTranslateItems(id)
    }


    @CrossOrigin
    @PostMapping("/deleteTranslateHistorys")
    fun deleteTranslateHistory(@RequestBody ids: List<String>): Boolean {
        for (id in ids) {
            translateHistoryMapper.deleteTranslateHistorys(id)
        }
        return true
    }

    @CrossOrigin
    @PostMapping("/revertTranslateHistorys")
    fun revertTranslateHistory(@RequestBody ids: List<String>): Boolean {
        for (id in ids) {
            var items = translateHistoryMapper.findTranslateItems(id)
            for (item in items) {
                when (item.elementType) {
                    ElementType.Element -> translateHistoryMapper.revertTranslateElement(item)
                    ElementType.Chapter -> translateHistoryMapper.revertTranslateChapter(item)
                    else -> println("error type : ${item}")
                }
            }
        }
        return true
    }

    @CrossOrigin
    @PostMapping("/revertTranslateItems")
    fun revertTranslateItems(@RequestBody ids: List<String>): Boolean {
        for (id in ids) {
            try {
                var item = translateHistoryMapper.getTranslateItem(id)
                if (item != null) {
                    when (item.elementType) {
                        ElementType.Element -> translateHistoryMapper.revertTranslateElement(item)
                        ElementType.Chapter -> translateHistoryMapper.revertTranslateChapter(item)
                        else -> println("error type : ${item}")
                    }
                }
            } catch (e:Exception) {
                println(e.message)
            }
        }
        return true
    }

}