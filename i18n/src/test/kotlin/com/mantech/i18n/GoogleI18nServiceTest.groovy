package com.mantech.i18n

import com.mantech.i18n.translate.ElementType
import com.mantech.i18n.translate.TranslateItem
import org.springframework.util.FileCopyUtils
import spock.lang.Specification

class GoogleI18nServiceTest extends Specification {
    GoogleI18nService googleI18nService;
    void setup() {
        googleI18nService = new GoogleI18nService()
        googleI18nService.init()
    }

    def "ToTsvContent"() {
        given:
        List<TranslateItem> translateItemList = new ArrayList<>();
        translateItemList.add(new TranslateItem(ElementType.Element, "R01", "한글번역 테스트"))
        translateItemList.add(new TranslateItem(ElementType.Element, "R02", "핫빗 설정"))
        translateItemList.add(new TranslateItem(ElementType.Element, "R03", "주식회사 맨텍"))
        translateItemList.add(new TranslateItem(ElementType.Element, "R04", "MCCS 에이전트"))

        when:
        String output = googleI18nService.toTsvContent(translateItemList)
        println("output:${output}");

        then:
        output.length() > 0
        when:
        FileCopyUtils.copy(new File(googleI18nService.uploadTsvFilePath), new File(googleI18nService.downloadTsvFilePath))

        translateItemList = googleI18nService.getTsvContent()
        for(TranslateItem item: translateItemList) {
            println("item:${item.itemId}, ${item.content}");
        }

        then:
        translateItemList.size() > 0
    }

    def "test getTsvContent"() {
        when:
        List<TranslateItem> translateItemList = googleI18nService.getTsvContent()
        for(TranslateItem item: translateItemList) {
            println("item:${item.itemId}, ${item.content}");
        }

        then:
        translateItemList.size() > 0
    }

    def "test Translate"() {
        expect:
        String translatedContent = googleI18nService.translate(content)
        println("source:${content}, translated:${translatedContent},expect:${result}");
        translatedContent.length() > 0

        where:
        content|result
//        "MCCS 실행 계정" | "Account running MCCS"
//        "추가 설정" | "Additional Settings"
//        "자세한 사항은" | "For more information,"
//        "# MCCS 삭제를 합니다. 자세한 내용은 MCCS 설치 매뉴얼의 <a class=reference href=https://docs.mantech.co.kr/r/viewer/book/5f9303ecca2d8472#4107bd339f5f61a9>5. 삭제</a><img src=/resource/image/web_viewer/link.png alt=새 창에서 링크 열기 width=10 height=10 />를 참고하십시오. LINEBREAKFOR3RABBITZ # C:\\\\Program Files 안에 MCCS 폴더를 삭제합니다. LINEBREAKFOR3RABBITZ # 새로 발행된 MCCS를 설치합니다. 자세한 내용은 설치 매뉴얼의 <a class=reference href=https://docs.mantech.co.kr/r/viewer/book/5f9303ecca2d8472#13638a3c0d4274c1>2. 설치</a><img src=/resource/image/web_viewer/link.png alt=새 창에서 링크 열기 width=10 height=10 />를 참고하십시오. LINEBREAKFOR3RABBITZ # 설치 완료 후 MCCS 구성을 새로 합니다." | ""
//        "# MCCS 삭제를 합니다. 자세한 내용은 MCCS 설치 매뉴얼의 <a class=reference href=https://docs.mantech.co.kr/r/viewer/book/5f9303ecca2d8472#4107bd339f5f61a9>5. 삭제</a><img src=/resource/image/web_viewer/link.png alt=\"OpenInNewWindow\" width=10 height=10 />를 참고하십시오. LINEBREAKFOR3RABBITZ # C:\\\\Program Files 안에 MCCS 폴더를 삭제합니다. LINEBREAKFOR3RABBITZ # 새로 발행된 MCCS를 설치합니다. 자세한 내용은 설치 매뉴얼의 <a class=reference href=https://docs.mantech.co.kr/r/viewer/book/5f9303ecca2d8472#13638a3c0d4274c1>2. 설치</a><img src=/resource/image/web_viewer/link.png alt=\"OpenInNewWindow\" width=10 height=10 />를 참고하십시오. LINEBREAKFOR3RABBITZ # 설치 완료 후 MCCS 구성을 새로 합니다." | ""
        "# MCCS 삭제를 합니다. 자세한 내용은 MCCS 설치 매뉴얼의 <a class=\"reference\" href=https://docs.mantech.co.kr/r/viewer/book/5f9303ecca2d8472#4107bd339f5f61a9>5. 삭제</a>를 참고하십시오.<img src=/resource/image/web_viewer/link.png width=10 height=10 /> 를 참고하십시오. LINEBREAKFOR3RABBITZ # C:\\Program Files 안에 MCCS 폴더를 삭제합니다. LINEBREAKFOR3RABBITZ"| ""
//        "맨텍" | "Mantech"
    }
}
