package com.mantech.i18n

import com.mantech.i18n.dao.BookMapper
import com.mantech.i18n.dao.ChapterElementMapper
import com.mantech.i18n.dao.ChapterMapper
import com.mantech.i18n.dao.TranslateHistoryMapper
import com.mantech.i18n.entity.ChapterElement
import com.mantech.i18n.translate.TranslateItem
import org.springframework.messaging.simp.SimpMessagingTemplate
import spock.lang.Specification

class ThreeRabbitzServiceTest extends Specification {
    ThreeRabbitzService threeRabbitzService;
    void setup() {
        ChapterElementMapper chapterElementMapper = Mock(ChapterElementMapper)
        BookMapper bookMapper = Mock(BookMapper)
        ChapterMapper chapterMapper = Mock(ChapterMapper)
        GoogleI18nService googleI18nService = Mock(GoogleI18nService)
        TranslateHistoryMapper translateHistoryMapper = Mock(TranslateHistoryMapper)
        SimpMessagingTemplate simpMessagingTemplate = Mock(SimpMessagingTemplate)
//        threeRabbitzService = new ThreeRabbitzService(bookMapper, chapterElementMapper, chapterMapper, googleI18nService, translateHistoryMapper);
        threeRabbitzService = new ThreeRabbitzService(bookMapper, chapterElementMapper, chapterMapper, googleI18nService, translateHistoryMapper, simpMessagingTemplate);
    }

    def "ContainsWrongNewLine"() {
        expect:
        isContainsWrongLine == threeRabbitzService.containsWrongNewLine(content)
        if(isContainsWrongLine) {
            def output = threeRabbitzService.replaceWrongNewLine(content)
            println(output);
        }

        where:
        content | isContainsWrongLine | afterReplace
        "0.Test \\ n \\ NAgent Batch" | true | ""
        "1.Create Cluster \\ n * Add FT Cluster \\ n * Add System \\ n * Import System \\ n * Update Agent Batch" | true | ""
        "2.You can see the status of each type of system at a glance. \\ NClick the  Add / Manage HA Cluster icon " | false | ""
        "3.just glance. \\NClick the  Add / Manage HA Cluster icon " | false | ""
        "4.just glance.\\ NClick the  Add / Manage HA Cluster icon " | false | ""
    }

    def "contains special character"() {
        expect:
        isContainsWrongLine == threeRabbitzService.containsSpecialCharacter(content)
        if(isContainsWrongLine) {
            def output = threeRabbitzService.replaceSpecialCharacter(content)
            println(output);
            afterReplace == output
        }

        where:
        content | isContainsWrongLine | afterReplace
        "0.click & lt; Settings | Please select a dashboard from Dashboard & gt; and" | true | "0.click <Settings | Please select a dashboard from Dashboard> and"
        "1.& lt; Control server IP address & gt; & lt; FQDN" | true | "1.<Control server IP address> <FQDN"
        "2. & amp; aaa" | true | "2. & aaa"
        "3.just glance. \\NClick the  Add / Manage HA Cluster icon " | false | ""
        "4.just glance.\\ NClick the  Add / Manage HA Cluster icon " | false | ""
        "5. For clusters, the & lt; System | Console & gt; Screen," | true | "5. For clusters, the & lt; System | Console & gt; Screen,"
        "6. & lt; Settings | Roles & amp; Privileges & gt; Enter the menu" | true | "& lt; Settings | Roles & amp; Privileges & gt; Enter the menu"
    }

    def "caontains image tag"() {
        expect:
        isContainsWrongLine == threeRabbitzService.containsWrongImgTag(content)
        if(isContainsWrongLine) {
            def output = threeRabbitzService.replaceWrongImgTag(content)
            println(output);
            afterReplace == output
        }
        where:
        content | isContainsWrongLine | afterReplace
        "click the <img src = / r / image / get / bcbd084cff069ab2 width = 18 height = 19 /> icon at the top" | true | "click the <img src =/r/image/get/bcbd084cff069ab2 width=18 height=19 /> icon at the top"
        "Set the <img src = / r / image / get / 39ae5f5ad63667a4 width = 86 height = 26 /> button on" | true | "Set the <img src = / r / image / get / 39ae5f5ad63667a4 width = 86 height = 26 /> button on"
    }

    def "test getTranslateItem"() {
        expect:
        ChapterElement element = new ChapterElement(id, content)
        TranslateItem translateItem = threeRabbitzService.getTranslateItem(element, "test")
        translateItem.origin == content
        translateItem.content.contains(threeRabbitzService.LINE_BREAK_REPLACE_CONTENT) == haveNewLine
        translateItem.translateId == "test"
        translateItem.itemId == id

        where:
        content | id | haveNewLine
        "just test" | "abcd" | false
        "한글테스트" | "abcd" | false
        """hello
I have new line""" | "newlineTest" | true
    }

    def "test replaceAltAttribute"() {
        expect:
        String output = threeRabbitzService.replaceAltAttribute(content)
        println(output);

        output.length() > 0

        where:
        content | expectResult
        "# MCCS 삭제를 합니다. 자세한 내용은 MCCS 설치 매뉴얼의 <a class=reference href=https://docs.mantech.co.kr/r/viewer/book/5f9303ecca2d8472#4107bd339f5f61a9>5. 삭제</a><img src=/resource/image/web_viewer/link.png alt=\"새 창에서 링크 열기\" width=10 height=10 />를 참고하십시오. LINEBREAKFOR3RABBITZ # C:\\\\\\\\Program Files 안에 MCCS 폴더를 삭제합니다. LINEBREAKFOR3RABBITZ # 새로 발행된 MCCS를 설치합니다. 자세한 내용은 설치 매뉴얼의 <a class=reference href=https://docs.mantech.co.kr/r/viewer/book/5f9303ecca2d8472#13638a3c0d4274c1>2. 설치</a><img src=/resource/image/web_viewer/link.png alt=\"새 창에서 링크 열기\" width=10 height=10 />를 참고하십시오. LINEBREAKFOR3RABBITZ # 설치 완료 후 MCCS 구성을 새로 합니다." | "abcd"
        "# MCCS 삭제를 합니다. 자세한 내용은 MCCS 설치 매뉴얼의 <a class=reference href=https://docs.mantech.co.kr/r/viewer/book/5f9303ecca2d8472#4107bd339f5f61a9>5. 삭제</a><img src=/resource/image/web_viewer/link.png alt=새 창에서 링크 열기 width=10 height=10 />를 참고하십시오. 3RABBITZLINEBREAK # C:\\\\Program Files 안에 MCCS 폴더를 삭제합니다. 3RABBITZLINEBREAK # 새로 발행된 MCCS를 설치합니다. 자세한 내용은 설치 매뉴얼의 <a class=reference href=https://docs.mantech.co.kr/r/viewer/book/5f9303ecca2d8472#13638a3c0d4274c1>2. 설치</a><img src=/resource/image/web_viewer/link.png alt=새 창에서 링크 열기 width=10 height=10 />를 참고하십시오. 3RABBITZLINEBREAK # 설치 완료 후 MCCS 구성을 새로 합니다." | ""
    }
}
