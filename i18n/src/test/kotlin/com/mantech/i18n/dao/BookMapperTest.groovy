package com.mantech.i18n.dao

import com.mantech.i18n.entity.Book
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

//@RunWith(SpringRunner.class)
@SpringBootTest
//@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = I18nApplication.class)
//@MybatisTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//@Import(BookMapper.class)
class BookMapperTest extends Specification {
    @Autowired
    BookMapper bookMapper;


    def "test findAllBooks"() {
        when:
        List<Book> books = bookMapper.findAllBooks();
        for(Book book: books) {
            println("${book.id}:${book.title}");
        }


        then:
        books.size() > 0
    }

    def "test findAllBook Info"() {
        when:
        Book book = bookMapper.getBookInfo("00f737753d4e5030")
        println(book.title);


        then:
        book != null
    }
}
