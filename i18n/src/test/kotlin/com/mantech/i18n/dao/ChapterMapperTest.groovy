package com.mantech.i18n.dao


import com.mantech.i18n.entity.Chapter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class ChapterMapperTest extends Specification {
    @Autowired
    ChapterMapper mapper;


    def "test findAllBooks"() {
        given:
        String bookId = "45f6f4f61635a58a"
        when:
        List<Chapter> items= mapper.findChapterById(bookId);
        for(Chapter item: items) {
            println("${item.id}:${item.title}");
        }


        then:
        items.size() > 0
    }

    def "test findAllBook Info"() {
        given:
        String id = "3a9303e9f218d879"
        when:
        Chapter chapter = mapper.getChapterInfo(id)
        println(chapter.title);


        then:
        chapter != null
    }
}
