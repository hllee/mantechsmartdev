package com.mantech.i18n.dao


import com.mantech.i18n.translate.TranslateItem
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class TranslateHistoryMapperTest extends Specification {
    @Autowired
    TranslateHistoryMapper mapper;


    def "test findAllBooks"() {
//        when:
//        List<TranslateHistory> items = mapper.findAllTranslateHistorys()
//        for(TranslateHistory item:items) {
//            println("${item.translateId}:${item.name}");
//        }
//        def size = items.size()
//
//        TranslateHistory translateHistory = new TranslateHistory("id1", "test")
//        mapper.addTranslateHistory(translateHistory)
//
//        items = mapper.findAllTranslateHistorys()
//        for(TranslateHistory item:items) {
//            println("${item.translateId}:${item.name}");
//        }
//        then:
//        items.size() == size + 1
//
//        when:
//        mapper.deleteTranslateHistorys(translateHistory.translateId)
//
//        items = mapper.findAllTranslateHistorys()
//        for(TranslateHistory item:items) {
//            println("${item.translateId}:${item.name}");
//        }
//        then:
//        items.size() == size
    }

    def "test findAllBook Info"() {
        given:
        String translateId = "id1"
        when:
        List<TranslateItem> items = mapper.findTranslateItems(translateId)
        for(TranslateItem item:items) {
            println("${item.translateId}:${item.content}, ${item.origin}");
        }
        def size = items.size()
        then:
        size == 0

       when:
       TranslateItem translateItem = new TranslateItem("aaa", "hello")
       translateItem.translateId = translateId
       translateItem.origin = "안녕하세요."

       int inserted = mapper.addTranslateItem(translateItem)
       then:
       inserted > 0

        when:

        items = mapper.findTranslateItems(translateId)
        for(TranslateItem item:items) {
            println("${item.translateId}:${item.content}, ${item.origin}");
        }
        then:
        items.size() == size + 1

        when:
        mapper.deleteTranslateItems(translateId)
        items = mapper.findTranslateItems(translateId)
        then:
        items.size() == 0
    }

    def "test findTranslateItemsByChapter"() {
        given:
        def chapterId = "68be29c8da51e358"

        when:
        List<TranslateItem> items = mapper.findTranslateItemsByChapter(chapterId)
        for(TranslateItem item:items) {
            println("${item.itemId}:${item.content}");
        }
        then:
        items.size() >0
    }
}
