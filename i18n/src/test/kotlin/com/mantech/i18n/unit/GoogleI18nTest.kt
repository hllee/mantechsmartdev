package com.mantech.i18n.unit
import com.mantech.i18n.GoogleI18nService
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class GoogleI18nTest {

    var googleI18nService: GoogleI18nService = GoogleI18nService()

    @BeforeTest
    fun init() {
        googleI18nService.init()
    }

//    @Test
    fun testSum() {
        var source = "MCCS 실행 계정";
        var expected = "Account running MCCS";
        var translatedContent = googleI18nService.translate(source);
        assertTrue(translatedContent.length > 0);
        println("source:${source}, to:${translatedContent}, expected:${translatedContent}")
        assertEquals(translatedContent, expected)
//        DefaultGroovyMethods.println(this, "source:" + getProperty("content") + ", translated:" + translatedContent + ",expect:" + DefaultGroovyMethods.invokeMethod(String.class, "valueOf", new Object[]{getProperty("result")}));
//        translatedContent.length() > 0;

    }
}