import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { BookProcessComponent } from './book-process/book-process.component';
import { AboutComponent } from './about/about.component';
import { ChaptersComponent } from './chapters/chapters.component';
import { TranslateHistoryComponent } from './translate-history/translate-history.component';
import { TranslateItemsComponent } from './translate-items/translate-items.component';

const routes: Routes = [
  {path: 'books', component: BooksComponent},
  {path: 'chapters/:id', component: ChaptersComponent},
  {path: 'process', component: BookProcessComponent},
  {path: 'translateHistorys', component: TranslateHistoryComponent},
  {path: 'translateItems/:id', component: TranslateItemsComponent},
  {path: 'about', component: AboutComponent},
  { path: '**', component: BooksComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
