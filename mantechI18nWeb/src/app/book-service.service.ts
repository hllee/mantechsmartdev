import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from './book';
import { tap, catchError } from 'rxjs/operators';
import { Chapter } from './chapter';
import { TranslateHistory } from './models/translate-history';
import { TranslateItem } from './models/translate-item';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private baseUrl = environment.apiUrl;
  private getBookListUrl = `${this.baseUrl}/books`;
  private getChapterListUrl = `${this.baseUrl}/chaptersByBook`;
  private getElementListUrl = `${this.baseUrl}/elementsByChapter`;
  private getTranslateHistoryUrl = `${this.baseUrl}/translateHistorys`;
  private getTranslateItemsUrl = `${this.baseUrl}/translateItemsByChapter`;
  private deleteTranslateHistoryUrl = `${this.baseUrl}/deleteTranslateHistorys`;
  private revertTranslateHistoryUrl = `${this.baseUrl}/revertTranslateHistorys`;

  constructor(private http: HttpClient) {}

  getBooks(): Observable<Book[]> {
    console.log('will get books');
    return this.http.get<Book[]>(this.getBookListUrl)
      .pipe(
        tap(s => console.log('fetched books, ${s}')),
      );
  }

  getChapters(id: string): Observable<Chapter[]> {
    console.log(`will get chapters ${id}`);
    return this.http.get<Chapter[]>(`${this.getChapterListUrl}/${id}`)
      .pipe(
        tap(s => console.log('fetched chapters, ${s}')),
      );
  }

  getElements(id: string): Observable<Element[]> {
    console.log(`will get chapters ${id}`);
    return this.http.get<Element[]>(`${this.getElementListUrl}/${id}`)
      .pipe(
        tap(s => console.log('fetched elements, ${s}')),
      );
  }
  getTranslateHistorys(): Observable<TranslateHistory[]> {
    console.log('will get translate historys');
    return this.http.get<TranslateHistory[]>(this.getTranslateHistoryUrl)
      .pipe(
        tap(s => console.log('fetched translate historys, ${s}')),
      );
  }

  getTranslateItems(id: string): Observable<TranslateItem[]> {
    console.log(`will get translate items ${id}`);
    return this.http.get<TranslateItem[]>(`${this.getTranslateItemsUrl}/${id}`)
      .pipe(
        tap(s => console.log('fetched translate items, ${s}')),
      );
  }

  translateChapter(id: string): Observable<Chapter[]> {
    const transtlateUri = `${this.baseUrl}/translatechapter/${id}`;
    console.log(`will call api for translate chapter:${id}, url:${transtlateUri}`);
    return this.http.post<Chapter[]>(transtlateUri, {});
  }

  translateBook(id: string): Observable<Chapter[]> {
    const transtlateUri = `${this.baseUrl}/translatebook/${id}`;
    console.log(`will call api for translate book:${id}, url:${transtlateUri}`);
    return this.http.post<Chapter[]>(transtlateUri, {});
  }

  deleteTranslateHistory(items: string[]): Observable<boolean> {
    return this.http.post<boolean>(this.deleteTranslateHistoryUrl, items);
  }

  revertTranslateHistory(items: string[]): Observable<boolean> {
    return this.http.post<boolean>(this.revertTranslateHistoryUrl, items);
  }

  revertTranslateItems(items: string[]): Observable<boolean> {
    const requestUrl = `${this.baseUrl}/revertTranslateItems`;
    return this.http.post<boolean>(requestUrl, items);
  }

}
