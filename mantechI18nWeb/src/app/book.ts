export class Book {
    id: string;
    title: string;
    project: string;
    creator: string;
    updated: Date;
}
