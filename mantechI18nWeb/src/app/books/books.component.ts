import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BookService } from '../book-service.service';
import { Book } from '../book';
import { ProgressSpinnerComponent } from '../progress-spinner/progress-spinner.component';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.sass']
})
export class BooksComponent implements OnInit {

  displayedColumns: string[] = ['project', 'creator', 'title', 'updated', 'action'];
  data: Book[] = [];
  dataSource = new MatTableDataSource(this.data);
  dialogRef: MatDialogRef<ProgressSpinnerComponent>;
  constructor(private bookService: BookService, private dialog: MatDialog) { }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  ngOnInit() {
    // this.dataSource.sort = this.sort;
    this.getBooks();
  }

  getBooks(): void {
    this.bookService.getBooks()
    .subscribe(books => {
      this.dataSource = new MatTableDataSource(books);
      this.dataSource.sort = this.sort;
    },
      (error) => {
        console.log(error);
      }
      );
  }

  translate(id: string, title: string): void {
    if (confirm(`매뉴얼 "${title}"을 번역하겠습니까? 번역하는 내용에 따라 몇분정도 소요될 수 있습니다.`)) {
      this.showProgressSpinner();
      this.bookService.translateBook(id).subscribe(chapters => {
        console.log(chapters);
        this.hideProgressSpinner();
        if (confirm(`매뉴얼 "${title}"을 번역완료 하였습니다. 새 탭에 webviewer로 열겠습니까?`)) {
              window.open(`https://docs.mantech.co.kr/r/viewer/book/${id}`, '_blank');
          }
    },
      (error) => {
        console.log(error);
        if (!!this.dialogRef) {
        this.hideProgressSpinner();
        }
      }
      );
    }
  }

  showProgressSpinner() {
    this.dialogRef = this.dialog.open(ProgressSpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
  }

  hideProgressSpinner() {
    if (!!this.dialogRef) {
    this.dialogRef.close();
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
