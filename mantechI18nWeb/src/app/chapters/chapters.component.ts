import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { BookService } from '../book-service.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Chapter } from '../chapter';
import { ProgressSpinnerComponent } from '../progress-spinner/progress-spinner.component';

@Component({
  selector: 'app-chapters',
  templateUrl: './chapters.component.html',
  styleUrls: ['./chapters.component.sass']
})
export class ChaptersComponent implements OnInit {

  private bookId: string;
  displayedColumns: string[] = ['checked', 'id', 'title', 'action'];
  data: Chapter[] = [];
  checked = false;
  dataSource = new MatTableDataSource(this.data);
  dialogRef: MatDialogRef<ProgressSpinnerComponent>;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private route: ActivatedRoute,
              private bookService: BookService,
              private dialog: MatDialog,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
        this.bookId = params.id;
        return this.getChapters();
      }
      );
  }

  getChapters() {
        this.bookService.getChapters(this.bookId)
          .subscribe(books => {
            this.dataSource = new MatTableDataSource(books);
            this.dataSource.sort = this.sort;
            return books;
          },
            (error) => {
              console.log(error);
            }
          );
  }

  translate(id: string, title: string): void {
    if (confirm(`매뉴얼 "${title}"을 번역하겠습니까? 번역하는 내용에 따라 몇분정도 소요될 수 있습니다.`)) {
      this.showProgressSpinner();
      this.bookService.translateChapter(id).subscribe(chapters => {
        console.log(chapters);
        this.hideProgressSpinner();
    },
      (error) => {
        console.log(error);
        if (!!this.dialogRef) {
        this.hideProgressSpinner();
        // window.open(url, '_blank');
        }
      }
      );
    }
  }

  showProgressSpinner() {
    this.dialogRef = this.dialog.open(ProgressSpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
  }

  hideProgressSpinner() {
    if (!!this.dialogRef) {
    this.dialogRef.close();
    }
  }

  checkAll(): void {
    this.checked = !this.checked;
    for (const item of this.data) {
      item.checked = this.checked;
    }
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
