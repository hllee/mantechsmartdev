export class TranslateHistory {
    translateId: string;
    date: Date;
    name: string;
    checked = false;
}
