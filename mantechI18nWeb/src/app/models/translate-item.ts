export class TranslateItem {
    elementType: string;
    itemId: string;
    content: string;
    origin: string;
    translateId: string;
    checked = false;
}
