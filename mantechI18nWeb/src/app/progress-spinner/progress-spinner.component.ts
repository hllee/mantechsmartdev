import { Component, OnInit } from '@angular/core';
import * as io from 'socket.io-client';
import { EventSocketService } from '../services/event-socket-service.service';
import { environment } from './../../environments/environment';
@Component({
  selector: 'app-progress-spinner',
  templateUrl: './progress-spinner.component.html',
  styleUrls: ['./progress-spinner.component.sass']
})
export class ProgressSpinnerComponent implements OnInit {
  private message = 'Just wait few minutes please...';
  socketio: any;
  private baseUrl = environment.apiUrl;
  constructor(private eventSocketService: EventSocketService) { }
  // constructor() { }

  ngOnInit() {
    console.log('start listner websocket');
    // this.socketio = io(`http://127.0.0.1:8081/socket`);
    this.socketio = io(`${this.baseUrl}`, {path: '/socket'});
    this.socketio.on('/events/translate', data => {
      console.log(data);
      this.message = data;
    });
    this.socketio.on('/event', data => {
      console.log(data);
      this.message = data;
    });
    this.socketio.on('/events', data => {
      console.log(data);
      this.message = data;
    });
  }

}
