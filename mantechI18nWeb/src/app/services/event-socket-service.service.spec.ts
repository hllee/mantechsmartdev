import { TestBed } from '@angular/core/testing';

import { EventSocketServiceService } from './event-socket-service.service';

describe('EventSocketServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventSocketServiceService = TestBed.get(EventSocketServiceService);
    expect(service).toBeTruthy();
  });
});
