import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EventSocketService {

  private baseUrl = environment.apiUrl;
  private socket: SocketIOClient.Socket;

  constructor() {
    // this.socket = io(`${this.baseUrl}/sockets`);
    this.socket = io(`${this.baseUrl}`, {path: '/socket'});
    // this.socket = io.connect(`${this.baseUrl}`, {path: '/socket'});
    // this.socket.connect();
    this.socket.on('/events/translate', msg => {
      console.log('/events/translate', msg);
      });
    // this.socket.on('events/translate', msg => {
    //   console.log('events/translate', msg);
    //   });
    // this.socket.on('/events', msg => {
    //   console.log('/events', msg);
    //   });
    // this.socket.on('events', msg => {
    //   console.log('events', msg);
    //   });
    // this.socket.on('events', msg => {
    //   console.log('events', msg);
    //   });
    // this.socket.on('events', msg => {
    //   console.log('events', msg);
    //   });
    // this.socket.on('translate', msg => {
    //   console.log('translate', msg);
    //   });
    console.log(this.socket);
  }

  // EMITTER
  sendMessage(msg: string) {
    this.socket.emit('sendMessage', { message: msg });
  }

  // HANDLER
  onNewMessage() {
    return Observable.create(observer => {
      this.socket.on('/events/translate', msg => {
        observer.next(msg);
      });
    });
  }
}
