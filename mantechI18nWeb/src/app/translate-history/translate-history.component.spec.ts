import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslateHistoryComponent } from './translate-history.component';

describe('TranslateHistoryComponent', () => {
  let component: TranslateHistoryComponent;
  let fixture: ComponentFixture<TranslateHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TranslateHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TranslateHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
