import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BookService } from '../book-service.service';
import { Book } from '../book';
import { ProgressSpinnerComponent } from '../progress-spinner/progress-spinner.component';
import { TranslateHistory } from '../models/translate-history';

@Component({
  selector: 'app-books',
  templateUrl: './translate-history.component.html',
  styleUrls: ['./translate-history.component.sass']
})
export class TranslateHistoryComponent implements OnInit {

  displayedColumns: string[] = ['checked', 'id', 'title', 'updated'];
  data: TranslateHistory[] = [];
  checked = false;
  dataSource = new MatTableDataSource(this.data);
  dialogRef: MatDialogRef<ProgressSpinnerComponent>;
  constructor(private bookService: BookService, private dialog: MatDialog) { }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  ngOnInit() {
    // this.dataSource.sort = this.sort;
    this.getBooks();
  }

  getBooks(): void {
    this.bookService.getTranslateHistorys()
    .subscribe(items => {
      this.data = items;
      this.dataSource = new MatTableDataSource(this.data);
      this.dataSource.sort = this.sort;
    },
      (error) => {
        console.log(error);
      }
      );
  }

  checkAll(): void {
    this.checked = !this.checked;
    for (const item of this.data) {
      item.checked = this.checked;
    }
  }

  deleteSelected(): void {
    const items: string[] = [];
    for (const item of this.data) {
      if (item.checked) {
        items.push(item.translateId);
      }
    }
    if (items.length > 0) {
      this.bookService.deleteTranslateHistory(items).subscribe(() => this.getBooks());
    }
  }

  revertSelected(): void {
    const items: string[] = [];
    for (const item of this.data) {
      if (item.checked) {
        items.push(item.translateId);
      }
    }
    if (items.length > 0) {
      this.bookService.revertTranslateHistory(items).subscribe(() => this.getBooks());
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
