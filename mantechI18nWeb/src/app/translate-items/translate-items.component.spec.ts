import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslateItemsComponent } from './translate-items.component';

describe('TranslateItemsComponent', () => {
  let component: TranslateItemsComponent;
  let fixture: ComponentFixture<TranslateItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TranslateItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TranslateItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
