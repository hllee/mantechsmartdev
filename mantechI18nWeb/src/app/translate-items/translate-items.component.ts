import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { BookService } from '../book-service.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Chapter } from '../chapter';
import { ProgressSpinnerComponent } from '../progress-spinner/progress-spinner.component';
import { TranslateItem } from '../models/translate-item';

@Component({
  selector: 'app-translate-items',
  templateUrl: './translate-items.component.html',
  styleUrls: ['./translate-items.component.sass']
})
export class TranslateItemsComponent implements OnInit {


  private chapterId: string;
  // displayedColumns: string[] = ['checked', 'id', 'content', 'origin', 'html'];
  displayedColumns: string[] = ['checked', 'id', 'content', 'origin'];
  data: TranslateItem[] = [];
  checked = false;
  dataSource = new MatTableDataSource(this.data);
  dialogRef: MatDialogRef<ProgressSpinnerComponent>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private route: ActivatedRoute,
              private bookService: BookService,
              private dialog: MatDialog,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.chapterId = params.id;
      return this.getData();
    }
    );
  }

  getData() {
    this.bookService.getTranslateItems(this.chapterId)
      .subscribe(items => {
        this.data = items;
        this.dataSource = new MatTableDataSource(items);
        this.dataSource.sort = this.sort;
        return items;
      },
        (error) => {
          console.log(error);
        }
      );
  }

  checkAll(): void {
    this.checked = !this.checked;
    for (const item of this.data) {
      item.checked = this.checked;
    }
  }

  revertSelected(): void {
    const items: string[] = [];
    for (const item of this.data) {
      if (item.checked) {
        items.push(item.itemId);
      }
    }
    if (items.length > 0) {
      this.bookService.revertTranslateItems(items).subscribe(() => this.getData());
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
