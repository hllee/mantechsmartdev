# mantechOvertime

연구소 야근보고소 생성 프로젝트입니다.

### how to build
1. ./gradlew build
2. ./gradlew copyCredential

## how to install 
1. cp build/libs /opt/mantechOvertime
2. sudo ln -s /opt/mantechOvertime/devlifeeasier-0.0.1-SNAPSHOT.jar /etc/init.d/mantechOvertime
3. sudo update-rc.d mantechOvertime defaults 
4. sudo service mantechOvertime start


## running service
* service 
http://10.30.99.199:30080/overtimeApplication

* server
10.30.99.199

* localtion
/opt/source/mantechOvertime





