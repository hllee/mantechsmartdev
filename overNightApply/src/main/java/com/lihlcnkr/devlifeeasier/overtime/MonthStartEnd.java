package com.lihlcnkr.devlifeeasier.overtime;

import lombok.Data;

import java.time.LocalDate;

@Data
public class MonthStartEnd {
    private final LocalDate startDate;
    private final LocalDate endDate;
}
