package com.lihlcnkr.devlifeeasier.overtime;


import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Data
public class OvertimeInfo {
    private final String email;
    private final String reason;
    private final boolean hadDinner;
    private final LocalDateTime finishTime;

    private final static String NO_DINNER_START_TIME = "17:30";
    private final static String HAD_DINNER_START_TIME = "18:00";
    private final static int WORK_FINISH_TIME_IN_MINITUE = 17 * 60 + 30;


    private final static DateTimeFormatter DAY_FORMAT = DateTimeFormatter.ofPattern("MM/dd");
    private final static DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern("HH:mm");
    //강제로 저녁식사 유무 따지지 않고 18:00부터 계산하는 flag.
    private boolean force_dinner_flag = true;

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if(!(o instanceof OvertimeInfo)) {
            return false;
        }
        OvertimeInfo overtimeInfo = (OvertimeInfo) o;
        if(!email.equals(overtimeInfo.getEmail())) {
            return false;
        }
        return toMonthDay().equals(overtimeInfo.toMonthDay());
    }

    @Override
    public int hashCode() {
        return toMonthDay().hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s,%b, %s,%s", email,hadDinner, finishTime,reason);
    }

    public String toMonthDay() {
        LocalDate localDate = finishTime.toLocalDate();
        return localDate.format(DAY_FORMAT);
    }

    public String getWorkDuration() {
        LocalTime localTime = finishTime.toLocalTime();
        String startTime = NO_DINNER_START_TIME;
        if(hadDinner || force_dinner_flag) {
            startTime = HAD_DINNER_START_TIME;
        }
        return String.format("%s~%s", startTime, localTime.format(TIME_FORMAT));
    }

    public float getOvertimeHour() {
        LocalTime localTime = finishTime.toLocalTime();
        //퇘근시간: 17:30. 차후 퇴근시간 변경 되면 이 부분 변경하면 된다.
        int startMin = WORK_FINISH_TIME_IN_MINITUE;
        if(hadDinner || force_dinner_flag) {
            startMin += 30;
        }

        int overtTimeFinishMin = localTime.getHour() * 60 + localTime.getMinute();
        int overTimeMin = overtTimeFinishMin - startMin;
        return (float) overTimeMin / 60;
    }


}