package com.lihlcnkr.devlifeeasier.overtime;

import lombok.Data;

@Data
public class UserInfo {
    private String userEmail;
    private String username;
}
