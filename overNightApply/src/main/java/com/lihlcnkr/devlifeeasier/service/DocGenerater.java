package com.lihlcnkr.devlifeeasier.service;

import com.lihlcnkr.devlifeeasier.overtime.OvertimeInfo;
import org.docx4j.XmlUtils;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.io3.Save;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.ContentAccessor;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.Text;
import org.docx4j.wml.Tr;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DocGenerater {

    //    private final static String DOC_TEMPLATE_PATH = "/opt/work/doc/mantech/overtime_template.docx";
//    private final static String DOC_TEMPLATE_PATH = "/overtime_template.docx";
    private String docTemplatePath = "overtime_template.docx";

    private final static DateTimeFormatter dayFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd");


    public synchronized File generate(String userName, List<OvertimeInfo> overtimeInfos) {
        File generateFile = null;
        if(overtimeInfos==null) {
            System.err.println("overtimeInfo is null");
            return null;
        }
        try(InputStream templateInput =
                    new FileInputStream(docTemplatePath)) {
            WordprocessingMLPackage wordMLPackage = null;
            try {
                wordMLPackage = WordprocessingMLPackage.load(templateInput);
            } catch (Docx4JException e) {
                e.printStackTrace();
            }
            MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();

            HashMap<String, String> mappings = new HashMap<>();
            mappings.put("userName", userName);
            mappings.put("fullDate", LocalDate.now().format(dayFormat));
            LocalDateTime startOvertime = LocalDateTime.now();
            LocalDateTime endOvertime = LocalDateTime.now();

            if(overtimeInfos.size() > 0) {
                startOvertime = overtimeInfos.get(0).getFinishTime();
                endOvertime = overtimeInfos.get(overtimeInfos.size() - 1).getFinishTime();
            }

            mappings.put("duration", String.format("%d/%d~%d/%d", startOvertime.getMonth().getValue(), startOvertime.getDayOfMonth(), endOvertime.getMonth().getValue(), endOvertime.getDayOfMonth()));
            double totalHour = overtimeInfos.stream().mapToDouble(OvertimeInfo::getOvertimeHour).sum();
            /**
             * 회사정책에 따라 소수점 이하는 내림정책을 사용한다. 1시간 미만인 것은 0시간으로 계산하도록 함. (SE기준과 동일)
             */
            //long reportHour = Math.round(totalHour);
            long reportHour = (long) Math.floor(totalHour);
            if (reportHour > 32L) {
                reportHour = 32L;
            }

            mappings.put("totalHour", String.format("%d", reportHour));

            Tbl parentTable = getFirstTable(documentPart);
            List<Object> parentRows = getAllElementFromObject(parentTable, Tr.class);
            Object parentTr = parentRows.get(6);
            Tbl tempTable = getFirstTable(parentTr);
            List<Object> rows = getAllElementFromObject(tempTable, Tr.class);
            if (rows.size() > 1) { //careful only tables with 1 row are considered here
                Tr templateRow = (Tr) rows.get(1);
                for (OvertimeInfo overtimeInfo : overtimeInfos) {
                    Map<String, String> replacements = convertToValMap(overtimeInfo);
                    addRowToTable(tempTable, templateRow, replacements);
                }
                assert tempTable != null;
                tempTable.getContent().remove(templateRow);
            }


            try {
                documentPart.variableReplace(mappings);
            } catch (JAXBException e) {
                e.printStackTrace();
            } catch (Docx4JException e) {
                e.printStackTrace();
            }
            try {
                generateFile = File.createTempFile("overtimeApplication", ".docx");
            } catch (IOException e) {
                e.printStackTrace();
            }
            try (OutputStream outputStream = new FileOutputStream(generateFile)) {
                Save saver = new Save(wordMLPackage);
                saver.save(outputStream);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (Docx4JException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(generateFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return generateFile;
    }

    private Tbl getFirstTable(Object obj) {
        Tbl tbl = null;
        List<Object> objs = getAllElementFromObject(obj, Tbl.class);
        if(objs.size()> 0) {
            tbl = (Tbl) objs.get(0);
        }
        return tbl;
    }


    private void addRowToTable(Tbl reviewTable, Tr templateRow, Map<String, String> replacements) {
        Tr workingRow = (Tr) XmlUtils.deepCopy(templateRow);
        List<?> textElements = getAllElementFromObject(workingRow, Text.class);
        for (Object object : textElements) {
            Text text = (Text) object;
            String replacementValue = (String) replacements.get(text.getValue());
            if (replacementValue != null)
                text.setValue(replacementValue);
        }
        reviewTable.getContent().add(workingRow);
    }

    private List<Object> getAllElementFromObject(Object obj, Class<?> toSearch)
    {
        List<Object> result = new ArrayList<>();
        if (obj instanceof JAXBElement) obj = ((JAXBElement<?>) obj).getValue();

        if (obj.getClass().equals(toSearch)) {
            result.add(obj);
        } else if (obj instanceof ContentAccessor)
        {
            List<?> children = ((ContentAccessor) obj).getContent();
            for (Object child : children)
            {
                result.addAll(getAllElementFromObject(child, toSearch));
            }

        }
        return result;
    }

    private static Map<String, String> convertToValMap(OvertimeInfo overtimeInfo) {
        Map<String, String> valMap = new HashMap<>();
        valMap.put("${row_detail}", overtimeInfo.getReason());
        valMap.put("row_detail", overtimeInfo.getReason());
        valMap.put("${row_date}", overtimeInfo.toMonthDay());
        valMap.put("${row_total}", String.format("%.1f",overtimeInfo.getOvertimeHour()));
        valMap.put("${row_time}", overtimeInfo.getWorkDuration());
        return valMap;
    }


    @Value("${overtime.template.path}")
    public void setDocTemplatePath(String docTemplatePath) {
        this.docTemplatePath = docTemplatePath;
    }

}
