package com.lihlcnkr.devlifeeasier.service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.lihlcnkr.devlifeeasier.overtime.MonthStartEnd;
import com.lihlcnkr.devlifeeasier.overtime.OvertimeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@EnableScheduling
@Service
public class GoogleSheetDownloadService {

    private final static Logger logger = LoggerFactory.getLogger(GoogleSheetDownloadService.class);

    private static final String APPLICATION_NAME =
            "mantechLifeEasyer";

    /** Global instance of the {@link FileDataStoreFactory}. */
    private static FileDataStoreFactory DATA_STORE_FACTORY;

    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY =
            JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport HTTP_TRANSPORT;

    private final static String SECRETKEY_FILE_PATH = "/mantechLifeEasyer.json";
    private final static String spreadsheetId = "1ycM3HeeXaglEWi2gkHKhUSjlI_HgUo9vi5wR6viWKT8";

    private final static DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("M/d/yyyy H:m:s");

    private static final List<String> SCOPES =
            Arrays.asList(SheetsScopes.SPREADSHEETS_READONLY);

    private Sheets sheetsService;
    private List<OvertimeInfo> monthOvertimeInfos = new ArrayList<>();

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
//            DATA_STORE_FACTORY = new FileDataStoreFactory(Paths.get(SECRETKEY_HOME, DATA_STORE_DIR_NAME).toFile());
            DATA_STORE_FACTORY = new FileDataStoreFactory(Paths.get(".").toFile());
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    private MonthStartEnd monthStartEnd;

    @Scheduled(cron = "${overtime.googlesheet.cron.expression}", zone="Asia/Seoul")
    @PostConstruct
    public void init() throws IOException {
        System.out.println("start collect overtime infomation");
        try(InputStream jsonIn =
                GoogleSheetDownloadService.class.getResourceAsStream(SECRETKEY_FILE_PATH)) {
            try (InputStreamReader fileReader = new InputStreamReader(jsonIn)) {
                GoogleClientSecrets clientSecrets =
                        GoogleClientSecrets.load(JSON_FACTORY, fileReader);

                // Build flow and trigger user authorization request.
                GoogleAuthorizationCodeFlow flow =
                        new GoogleAuthorizationCodeFlow.Builder(
                                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                                .setDataStoreFactory(DATA_STORE_FACTORY)
                                .setAccessType("offline")
                                .build();

                LocalServerReceiver localReceiver = new LocalServerReceiver.Builder().setPort(30081).build();
                Credential credential = new AuthorizationCodeInstalledApp(
                        flow, localReceiver).authorize("user");

                sheetsService = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                        .setApplicationName(APPLICATION_NAME)
                        .build();


                LocalDate localDate = LocalDate.now();
                LocalDate startLocalDate;
                LocalDate endLocalDate;
                startLocalDate = localDate.minusMonths(1).withDayOfMonth(20);
                endLocalDate = localDate.minusMonths(0).withDayOfMonth(21);
                monthStartEnd = new MonthStartEnd(startLocalDate, endLocalDate);
                getDatas();
            }
        }
    }

    public List<OvertimeInfo> getDatas() {
        List<OvertimeInfo> overtimeInfos = new ArrayList<>();
        String range = "Form Responses 1!A2:D";
        ValueRange response = null;
        try {
            response = sheetsService.spreadsheets().values()
                    .get(spreadsheetId, range)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<List<Object>> values = response.getValues();
        if (values == null || values.size() == 0) {
            logger.info("no data");
        } else {
            for (List row : values) {
                // Print columns A and E, which correspond to indices 0 and 4.
                //System.out.printf("%s, %s, %s\n", row.get(0), row.get(1), row.get(2));
                try {
                    OvertimeInfo overtimeInfo = convertToOvertimeInfo(row);
                    if (overtimeInfo != null) {
                        overtimeInfos.add(overtimeInfo);
                    }
                } catch (Exception e) {
                    logger.error("parsing data error:{}", e.getMessage());
                }
            }
        }

        this.monthOvertimeInfos = overtimeInfos.stream().filter(s -> {
            LocalDate localDate = s.getFinishTime().toLocalDate();
            return localDate.isAfter(monthStartEnd.getStartDate()) && localDate.isBefore(monthStartEnd.getEndDate());
            }
        ).collect(Collectors.toList());
        return this.monthOvertimeInfos;
    }

    public static OvertimeInfo convertToOvertimeInfo(List<Object> row) {
        if(row != null && row.size() > 3) {
            String dateData = (String) row.get(0);
            LocalDateTime date = LocalDateTime.parse(dateData, dateFormat);
            String reason = (String) row.get(1);
            String email = (String) row.get(2);
            if (email == null || !email.contains("@")) {
                return null;
            }
            String hadDinnerInfo = (String) row.get(3);
            boolean hadDinner = true;
            if (hadDinnerInfo != null && hadDinnerInfo.contains("연속")) {
                hadDinner = false;
            }
            return new OvertimeInfo(email, reason, hadDinner, date);
        } else {
            logger.error("row size is smaller than 4, row:{}", row);
            return null;
        }
    }

    public List<OvertimeInfo> getByEmail(String email) {
        return this.monthOvertimeInfos.stream()
                .filter(s -> s.getEmail().equals(email))
                .sorted(Comparator.comparing(OvertimeInfo::getFinishTime))
                .collect(Collectors.toList());
    }
}
