package com.lihlcnkr.devlifeeasier.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

@Component
public class LocalGitService {

    private String localGitPath;
    private String scriptPath;
    private String userId;
    private final static String newIssueScript = "gitNewBranch.sh";

    @Autowired
    public void setLocalGitPath(@Value("${local.git.home.path}") String localGitPath) {
        this.localGitPath = localGitPath;
    }


    public void createNewIssue(String issueId) {
        try {
            String scriptFullPath = Paths.get(scriptPath, newIssueScript).toAbsolutePath().toString();
            ProcessBuilder pb = new ProcessBuilder(scriptFullPath, String.format("%s/%s",userId, issueId));
            pb.directory(new File(localGitPath));
            Process p = pb.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    public void setUserId(@Value("${git.user.id}") String userId) {
        this.userId = userId;
    }

    @Autowired
    public void setScriptPath(@Value("${local.script.path}") String scriptPath) {
        this.scriptPath = scriptPath;
    }
}
