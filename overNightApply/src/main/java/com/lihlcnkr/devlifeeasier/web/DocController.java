package com.lihlcnkr.devlifeeasier.web;

import com.lihlcnkr.devlifeeasier.overtime.OvertimeInfo;
import com.lihlcnkr.devlifeeasier.overtime.UserInfo;
import com.lihlcnkr.devlifeeasier.service.DocGenerater;
import com.lihlcnkr.devlifeeasier.service.GoogleSheetDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
public class DocController {

    private final GoogleSheetDownloadService googleSheetDownloadService;
    private final DocGenerater docGenerater;

    @Autowired
    public DocController(DocGenerater docGenerater, GoogleSheetDownloadService googleSheetDownloadService) {
        this.docGenerater = docGenerater;
        this.googleSheetDownloadService = googleSheetDownloadService;
    }


    @PostMapping("/generateDoc")
    public ResponseEntity<Resource> generateDoc(UserInfo userInfo, HttpServletResponse httpServletResponse) {
        if(userInfo == null | userInfo.getUserEmail() == null || userInfo.getUsername() == null) {
            try {
                httpServletResponse.setHeader("Location", "/");
                httpServletResponse.sendRedirect("/");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String userEmail = userInfo.getUserEmail();
        String userName = userInfo.getUsername();

        List<OvertimeInfo> overtimeInfos = googleSheetDownloadService.getByEmail(userEmail);
        File file = docGenerater.generate(userName, overtimeInfos);

        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = null;
        try {
            resource = new ByteArrayResource(Files.readAllBytes(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok()
                .header("Content-Disposition", "attachment; filename=overtimeApplication.docx")
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }


}
