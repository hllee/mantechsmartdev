package com.lihlcnkr.devlifeeasier.web;

import com.lihlcnkr.devlifeeasier.service.LocalGitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/git")
public class GitController {


    final private LocalGitService localGitService;

    @Autowired
    public GitController(LocalGitService localGitService) {
        this.localGitService = localGitService;
    }

    @RequestMapping(value="/{issueId}", method= RequestMethod.PUT)
    public boolean helloWorld(@PathVariable String issueId) {
        localGitService.createNewIssue(issueId);
        return true;
    }

}
