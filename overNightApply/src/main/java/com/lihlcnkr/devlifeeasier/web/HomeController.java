package com.lihlcnkr.devlifeeasier.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class HomeController {


    @RequestMapping(value={"", "/", "/home"})
    public String welcome(Map<String, Object> model) {
        model.put("message", "welcome");
        return "home";
    }

    @RequestMapping("/overtimeApplication")
    public String overtimeApplication(Map<String, Object> model) {
        model.put("message", "welcome");
        return "overtime";
    }
}
