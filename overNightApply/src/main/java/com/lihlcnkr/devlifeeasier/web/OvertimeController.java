package com.lihlcnkr.devlifeeasier.web;

import com.lihlcnkr.devlifeeasier.overtime.OvertimeInfo;
import com.lihlcnkr.devlifeeasier.service.DocGenerater;
import com.lihlcnkr.devlifeeasier.service.GoogleSheetDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value="/overtimes")
public class OvertimeController {


    private final GoogleSheetDownloadService googleSheetDownloadService;
    private final DocGenerater docGenerater;

    @Autowired
    public OvertimeController(GoogleSheetDownloadService googleSheetDownloadService, DocGenerater docGenerater) {
        this.googleSheetDownloadService = googleSheetDownloadService;
        this.docGenerater = docGenerater;
    }

    @RequestMapping(value="/{userEmail}", method= RequestMethod.GET)
    public List<OvertimeInfo> getOvertimes(@PathVariable String userEmail) {
        return googleSheetDownloadService.getByEmail(userEmail);
    }
}
