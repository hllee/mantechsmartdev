package com.lihlcnkr.devlifeeasier.web;

import com.lihlcnkr.devlifeeasier.service.LocalGitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/setting")
public class SettingController {


    final private LocalGitService localGitService;

    @Autowired
    public SettingController(LocalGitService localGitService) {
        this.localGitService = localGitService;
    }

    @RequestMapping(value="/{issueId}", method= RequestMethod.PUT)
    public boolean helloWorld(@PathVariable String issueId) {
        localGitService.createNewIssue(issueId);
        return true;
    }

}
