package com.lihlcnkr.devlifeeasier.service

import com.lihlcnkr.devlifeeasier.overtime.OvertimeInfo
import spock.lang.Specification

class DocGeneraterTest extends Specification {

    DocGenerater docGenerater;

    def setup() {
        docGenerater = new DocGenerater();
    }

    def "test generate"() {
        given:
        String username = "lihlcnkr";
        List<OvertimeInfo> overtimeInfos = new ArrayList<>();
        overtimeInfos.add(new OvertimeInfo("hllee@mantech.co.kr", "hello", false, new Date()));
        overtimeInfos.add(new OvertimeInfo("hllee@mantech.co.kr", "test", true, new Date()));
        overtimeInfos.add(new OvertimeInfo("hllee@mantech.co.kr", "just test..", false, new Date()));


        when:
        docGenerater.generate(username, overtimeInfos);

        then:
        1==1
    }
}
