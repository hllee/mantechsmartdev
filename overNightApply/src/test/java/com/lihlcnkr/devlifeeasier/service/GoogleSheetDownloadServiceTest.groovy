package com.lihlcnkr.devlifeeasier.service

import com.lihlcnkr.devlifeeasier.overtime.OvertimeInfo

class GoogleSheetDownloadServiceTest extends spock.lang.Specification {

    GoogleSheetDownloadService googleSheetDownloadService;
    DocGenerater docGenerater;
    def setup() {
        googleSheetDownloadService = new GoogleSheetDownloadService();
        googleSheetDownloadService.init();
        docGenerater = new DocGenerater();
    }

    def "GetDatas"() {
        when:
        List<OvertimeInfo> overtimeInfoes = googleSheetDownloadService.getDatas();
        for(int i=0; i< Integer.min(50, overtimeInfoes.size());i++) {
            OvertimeInfo overtimeInfo = overtimeInfoes.get(i);
            println(overtimeInfo);
        }

        then:
        overtimeInfoes.size() > 0

        when:
        println("==================================");
        println("==================================");
        println("==================================");
        overtimeInfoes = googleSheetDownloadService.getByEmail("hllee@mantech.co.kr")
        overtimeInfoes.stream().each {s -> println(s)}

        then:
        overtimeInfoes.size() > 0

        when:
        docGenerater.generate("hllee", overtimeInfoes);

        then:
        1==1
    }
}
